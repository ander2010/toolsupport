﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace ToolsSoport.Models.Paginas
{
    public class PageRepository : IPageRepository
    {


        ToolSoportContext context;
        public PageRepository(ToolSoportContext ctx)
        {
            context = ctx;
        }




       

        public IEnumerable<Page> Pages => context.Paginas;

       

        public void AddPages(Page e)
        {
            context.Paginas.Add(e);
            context.SaveChanges();
        }

 

        public void DeletePages(int PageID)
        {
            Page e = new Page() { Id = PageID };
            context.Paginas.Remove(e);

            context.SaveChanges();
        }

      



     

        public void EditPages(Page e)
        {
            Page dbEntry = context.Paginas.FirstOrDefault(o => o.Id == e.Id);
            if (dbEntry != null)
            {
                // dbEntry.CreationDate = e.CreationDate;
                dbEntry.Description = e.Description;
                
                dbEntry.CreationDate = e.CreationDate;
                dbEntry.ModificationDate = e.ModificationDate;
                dbEntry.PermisoId = e.PermisoId;
                dbEntry.CategoriesId = e.CategoriesId;
                dbEntry.IsActive = e.IsActive;

                dbEntry.Name = e.Name;
                context.SaveChanges();
                //dbEntry. = e.Description;
            }
        }

  

        public Page GetPagesById(int PageID)
        {
            Page e = context.Paginas.FirstOrDefault(t => t.Id == PageID);
            return e;
        }
    }
}
