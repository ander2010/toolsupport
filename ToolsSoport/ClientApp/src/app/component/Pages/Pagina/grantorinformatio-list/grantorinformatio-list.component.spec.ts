import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GrantorinformatioListComponent } from './grantorinformatio-list.component';

describe('GrantorinformatioListComponent', () => {
  let component: GrantorinformatioListComponent;
  let fixture: ComponentFixture<GrantorinformatioListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GrantorinformatioListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GrantorinformatioListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
