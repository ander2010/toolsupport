import { Component, OnInit } from '@angular/core';
import { StorageService } from '../services/storage.service';
import { Router } from '@angular/router';
import { UsuarioService } from '../../app/services/usuario.service';


@Component({
  selector: 'app-nav-menu',
  templateUrl: './nav-menu.component.html',
  styleUrls: ['./nav-menu.component.css']
})
export class NavMenuComponent implements OnInit{
  isExpanded = false;
  ListMenu: any;
  login: boolean = false;
    ListadoPagina: any;


  collapse() {
    this.isExpanded = false;
  }

  toggle() {
    this.isExpanded = !this.isExpanded;
  }


  ngOnInit(): void {
    let t = this.storagesrevice.isAuthenticated();
   
   
   
   // this.usuarioservice.ListarPagina().subscribe(t => this.ListadoPagina = t);
   // console.info(this.ListadoPagina);

   


    if (t) {
      this.login = true;
      this.usuarioservice.ListarPagina().subscribe(t => this.ListadoPagina = t);
    } else {
      this.login = false;
    }



      // Cuando le dás click muestra #content
      //$('.btn-show-alert').click(function () {
      //  $("#content").toggleClass("hide");
      //});

      // Simular click
   
   



  }


  constructor(private storagesrevice: StorageService, private router: Router, private usuarioservice: UsuarioService) {

    let t = this.storagesrevice.isAuthenticated();

    if (t) {
      this.login = true;
      this.usuarioservice.ListarPagina().subscribe(t => this.ListadoPagina = t);
    } else {
      this.login = false;
    }
  }

  CloseSession() {
    this.login = false;
   
    this.storagesrevice.logout();

    //console.info("Llego");
  }
}
