import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ApplicationService } from '../../../services/application.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'applicationson-forms-add-eddit',
  templateUrl: './applicationson-forms-add-eddit.component.html',
  styleUrls: ['./applicationson-forms-add-eddit.component.css']
})
export class ApplicationsonFormsAddEdditComponent implements OnInit {

  applicationsSon: FormGroup;
  tipoApplicativos: any;
  titulo: string;
  parametro: string;

  constructor(private applicationservice: ApplicationService, private ruta: Router, private activateroute: ActivatedRoute) {
    this.applicationsSon = new FormGroup({

      "id": new FormControl("0", Validators.required),
      "name": new FormControl("", Validators.required),
      "description": new FormControl("", Validators.required),
      "url": new FormControl("", Validators.required),
      "applicationid": new FormControl("", Validators.required),


    });
    this.activateroute.params.subscribe(parametro => {
      this.parametro = parametro["id"];
      if (this.parametro == "id") {
        this.titulo = "Agregando un  nuevo aplicativo"

      }
      else {

        this.titulo = "Editando un nuevo aplicativo";

        this.applicationservice.GetApplicativePage(this.parametro).subscribe(t => {

          this.applicationsSon.controls["description"].setValue(t.descripcion);
          this.applicationsSon.controls["name"].setValue(t.nombre);
          this.applicationsSon.controls["id"].setValue(t.id);

        });
      }
    });

  }

  ngOnInit() {

    this.applicationservice.listarApplicative().subscribe(t => this.tipoApplicativos = t);
    if (this.parametro == "id") {
      this.titulo = "Agregando una Pagina"

    }
    else {

      this.applicationservice.GetApplicativePage(this.parametro).subscribe(t => {

        this.applicationsSon.controls["description"].setValue(t.descripcion);
        this.applicationsSon.controls["name"].setValue(t.nombre);
        this.applicationsSon.controls["id"].setValue(t.id);
      //  var Applications = t.listapagina.map(tt => tt.id);
      //pintar la información
      //setTimeout(() => {
      //  var checks = document.getElementsByClassName("check");
      //  var ncheck = checks.length;
      //  var check;
      //  for (var i = 0; i < ncheck; i++) {
      //    check = checks[i];
      //    var indice = 0
      //    if (indice > -1) {
      //      check.checked = true;

        //  }
        //}


      //}, 500);
      //    this.persona.controls["apMaterno"].setValue(t.apMaterno);
      //    this.persona.controls["telefono"].setValue(t.telefono);
      //    this.persona.controls["correo"].setValue(t.correo);
      //    this.persona.controls["fechanacimiento"].setValue(t.fechaCadena);

      });


    }
  }

  guardarDatos() {


    this.applicationservice.guardarDatosSon(this.applicationsSon.value).subscribe(t => {

      this.ruta.navigate(['/Pages']);
    });

  }
}
