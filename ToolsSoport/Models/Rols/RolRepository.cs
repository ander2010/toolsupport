﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ToolsSoport.Models;

namespace ToolsSoport.Models.Rols
{
    public class RolRepository : IRolRepository
    {

        ToolSoportContext context;
        public RolRepository(ToolSoportContext ctx)
        {
            context = ctx;
        }


        public IEnumerable<UserRol> Roles => context.Roles;

        public void AddRol(UserRol e)
        {
            context.Add(e);
            context.SaveChanges();
        }

        public void DeleteRol(int RolID)
        {

            UserRol e = new UserRol() { Id = RolID };
            context.Roles.Remove(e);
            context.SaveChanges();
        }

        public void EditRol(UserRol e)
        {
            UserRol dbEntry = context.Roles.FirstOrDefault(o => o.Id == e.Id);
            if (dbEntry != null)
            {
                dbEntry.Name = e.Name;
                dbEntry.Description = e.Description;
                dbEntry.IsActive = e.IsActive;

            }
            context.SaveChanges();
        }

        public UserRol GetRolById(int RolID)
        {

            UserRol e = context.Roles.FirstOrDefault(t => t.Id == RolID);
            return e;
        }
    }
}
