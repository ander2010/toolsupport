﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using ToolsSoport.Models.Applicative;
using ToolsSoport.Models.ApplicativeRoleUserPages;
using ToolsSoport.Models.Auditors;
using ToolsSoport.Models.Categories;
using ToolsSoport.Models.Paginas;
using ToolsSoport.Models.Permiso;
using ToolsSoport.Models.PersonaRol;
using ToolsSoport.Models.Rols;
using ToolsSoport.Models.Usuarios;

namespace ToolsSoport.Models
{
    public class ToolSoportContext: DbContext
    {
        public DbSet<UserRol> Roles { get; set; }
        public DbSet<Permisos> Permisos { get; set; }
        public DbSet<PermisosRol> PermisosRoles { get; set; }
        public DbSet<Users> Usuarios { get; set; }
        public DbSet<Auditor> Auditor { get; set; }


        public DbSet<ApplicativeUserRoles> ApplicativeUserRoles { get; set; }
        public DbSet<Page> Paginas { get; set; }
        public DbSet<Applicatives> Applicatives { get; set; }

        public DbSet<Categorie> Categories { get; set; }
        public DbSet<ApplicativeRoleUserPage> ApplicativeRoleUserPages { get; set; }

        



        public ToolSoportContext()
        {

        }
        public ToolSoportContext(DbContextOptions<ToolSoportContext> options) : base(options)
        {


        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);
            IConfigurationRoot Configuration;
            Configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json")
                .Build();
            optionsBuilder.UseSqlServer(Configuration["DefaultConnection"]);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);


            //modelBuilder.Entity("VolantesManager.Models.Notificaciones.Notificacion").HasData(
            //    new Notificacion { id = 1, NombreNotificacion = "Correo Electronico", Descripcion = "ES una forma nueva de notifiaciones", Bhabilitado = true },
            //      new Notificacion { id = 2, NombreNotificacion = "SMS", Descripcion = "Los sms son cool", Bhabilitado = true },
            //        new Notificacion { id = 3, NombreNotificacion = "WhatsApp  ", Descripcion = "Mensajeria instantanea usando whatsapp", Bhabilitado = true }
            //    );

            //Seeker.AddSeeKRole(modelBuilder);
            //Seeker.AddSeeKPersona(modelBuilder);
            //Seeker.AddSeeKEventos(modelBuilder);

        }


    }
}
