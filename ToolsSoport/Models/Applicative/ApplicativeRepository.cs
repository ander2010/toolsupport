﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ToolsSoport.Models.Applicative
{
    public class ApplicativeRepository : IApplicativeRepository
    {


        ToolSoportContext context;
        public ApplicativeRepository(ToolSoportContext ctx)
        {
            context = ctx;
        }


        public IEnumerable<Applicatives> Applicatives => context.Applicatives;

        public void AddApplicatives(Applicatives e)
        {
            context.Applicatives.Add(e);
            context.SaveChanges();
        }

        public void DeleteApplicatives(int ApplicativeID)
        {
            Applicatives e = new Applicatives() { Id = ApplicativeID };
            context.Applicatives.Remove(e);

            context.SaveChanges();
        }

        public void EditApplicatives(Applicatives e)
        {
            Applicatives dbEntry = context.Applicatives.FirstOrDefault(o => o.Id == e.Id);
            if (dbEntry != null)
            {
               // dbEntry.CreationDate = e.CreationDate;
                dbEntry.Description = e.Description;

                dbEntry.Name = e.Name;

                //dbEntry. = e.Description;
            }
        }

            public Applicatives GetApplicativesById(int ApplicativeID)
        {
            Applicatives e = context.Applicatives.FirstOrDefault(t => t.Id == ApplicativeID);
            return e;
        }
    }
}
