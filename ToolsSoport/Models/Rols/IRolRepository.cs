﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ToolsSoport.Models.Rols
{
  public  interface IRolRepository
    {


        IEnumerable<UserRol> Roles { get; }

        void AddRol(UserRol e);

        void EditRol(UserRol e);

        void DeleteRol(int RolID);
        UserRol GetRolById(int RolID);
    }
}
