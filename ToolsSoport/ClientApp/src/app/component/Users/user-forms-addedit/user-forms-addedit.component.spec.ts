import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserFormsAddeditComponent } from './user-forms-addedit.component';

describe('UserFormsAddeditComponent', () => {
  let component: UserFormsAddeditComponent;
  let fixture: ComponentFixture<UserFormsAddeditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserFormsAddeditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserFormsAddeditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
