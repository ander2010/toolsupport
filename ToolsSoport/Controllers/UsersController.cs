﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ToolsSoport.Helpers;
using ToolsSoport.Models;
using ToolsSoport.Models.ApiRestViewModel;
using ToolsSoport.Models.Applicative;
using ToolsSoport.Models.ApplicativeRoleUserPages;
using ToolsSoport.Models.Auditors;
using ToolsSoport.Models.Categories;
using ToolsSoport.Models.Paginas;
using ToolsSoport.Models.Permiso;
using ToolsSoport.Models.PersonaRol;
using ToolsSoport.Models.Rols;
using ToolsSoport.Models.Usuarios;

namespace ToolsSoport.Controllers
{
   
    public class UsersController : ControllerBase
    {
        
        IAuditorRepository todoItemsAuditor { get; set; }
      
        IApplicativeRoleUserPageRepository todoItemsApplicativeRoleUserPage{ get; set; }

        private IUsuarioRepository TodoItems { get; set; }
        private IApplicativeRepository TodoItemsAplicative { get; set; }

        private IPermisoRolRepository TodoPermisoRol { get; set; }
        private IPermisoRepository TodoPermiso { get; set; }
       
        private IRolRepository TodoRol { get; set; }

        private ICategoriesRespository TodoCategories { get; set; }
        private IPageRepository TodoPages { get; set; }
        private IApplicativeUserRolesRepository TodoUserRolAplicative { get; set; }
        
        public UsersController(IUsuarioRepository usuarioRepository, IPageRepository TodoPages,IApplicativeRoleUserPageRepository todoItemsApplicativeRoleUserPage, ICategoriesRespository TodoCategories,IAuditorRepository todoItemsAuditores, IApplicativeRepository TodoItemsAplicative,IPermisoRolRepository TodoPaginaRoles, IPermisoRepository TodoPaginas, IRolRepository TodoRoles, IApplicativeUserRolesRepository TodoUserRol)
        {
            TodoItems = usuarioRepository;
            this.TodoItemsAplicative = TodoItemsAplicative;
            this.TodoPermisoRol = TodoPaginaRoles;
            this.TodoUserRolAplicative = TodoUserRol;
            this.TodoPermiso = TodoPaginas;
            this.TodoRol = TodoRoles;
            todoItemsAuditor = todoItemsAuditores;
            this.TodoPages = TodoPages;
            this.TodoCategories = TodoCategories;
            this.todoItemsApplicativeRoleUserPage = todoItemsApplicativeRoleUserPage;

        }

     
        [HttpGet]
        [Route("api/Users/RoleList")]
        public IEnumerable<RolViewModel> RoleList()
        {

            List<RolViewModel> listaTipoUsuario = (from tipousuario in TodoRol.Roles
                                                   where tipousuario.IsActive == true
                                                   select new RolViewModel
                                                   {

                                                       id = tipousuario.Id,
                                                       name = tipousuario.Name
                                                   }

                                                        ).ToList();
            return listaTipoUsuario;


        }


        [HttpGet]
        [Route("api/Users/UserList")]
        public IEnumerable<UsuarioViewModel> UserList()
        {


            List<UsuarioViewModel> listaTipoUsuario = (from usuario in TodoItems.Usuarios
                                                       
                                                       where usuario.IsActive == true
                                                       select new UsuarioViewModel
                                                       {
                                                           id = usuario.id,
                                                            email=usuario.Email,
                                                         
                                                           habilitado = usuario.IsActive,
                                                            
                                                       }

                                                        ).ToList();
            return listaTipoUsuario;


        }

    
        [HttpPost]
        [Route("api/Users/PublicLogin")]
       
        public UserSessionModel PublicLogin([FromBody] PublicLoginModel loginData)
        {
            // Inicializa modelo de respuesta del método actual
            UserSessionModel returnModel = new UserSessionModel();
            // Consumo de servicio

            string cifradaPassword =CifrarPassword(loginData.password.ToString());

            bool dataResponse =  TodoItems.login(loginData.user, cifradaPassword);
            try
            {
                // Validación si la petición fué exitosa.
                if (dataResponse)
                {

                    returnModel.user = TodoItems.Usuarios.Where(t => t.Email == loginData.user).FirstOrDefault();
                    returnModel.userId = TodoItems.Usuarios.Where(t => t.Email == loginData.user).FirstOrDefault().id.ToString();
                    int usuario = TodoItems.Usuarios.Where(t => t.Email == loginData.user).FirstOrDefault().id;
                   // string rol = TodoUserRolAplicative.ApplicativeUserRoles.Where(t => t.UserId == usuario).FirstOrDefault().RoleId.ToString();
                                       
                    HttpContext.Session.SetString("usuario", returnModel.userId.ToString());
                   //HttpContext.Session.SetString("tipoUsuario", rol); //arreglar mas adelante

                    returnModel.sessionToken ="12000122501225";
                    returnModel.message = "Logueado Correctamente";
                    returnModel.succes = true;

                    //AuditorSave(OperationsController.UsersController_PublicLogin,"http//publiclogin", OperationsDescription.UsersController_PublicLogin);


                }
                else
                {
                    // Si la respuesta no fué exitosa asignamos los datos de respuesta.
                    returnModel.succes = false;
                    returnModel.message = "Error al tratar de loguearte";
                    //AuditorSave(OperationsController.UsersController_PublicLogin, "http//publiclogin", "Error al tratar de loguearte");

                }
            }
            catch (Exception error)
            {
                returnModel.succes = false;
                returnModel.message = error.Message;
            }
            return returnModel;
        }


        //[HttpGet]
        //[Route("api/Usuario/FiltradoUsuariotipo/{tipo?}")]
        //public IEnumerable<UsuarioViewModel> FiltradoUsuariotipo(int tipo)
        //{
        //    using (BDRestauranteContext bd = new BDRestauranteContext())
        //    {
        //        List<UsuarioViewModel> listaTipoUsuario = (from usuario in bd.Usuario
        //                                                   join Persona in bd.Persona
        //                                                   on usuario.Iidpersona equals Persona.Iidpersona
        //                                                   join TipoUsuario in bd.TipoUsuario on usuario.Iidtipousuario
        //                                                   equals TipoUsuario.Iidtipousuario
        //                                                   where usuario.Bhabilitado == 1 && usuario.Iidtipousuario == tipo
        //                                                   select new UsuarioViewModel
        //                                                   {

        //                                                       Id = usuario.Iidusuario,
        //                                                       nombretipoUsuario = usuario.Nombreusuario,
        //                                                       habilitado = usuario.Bhabilitado.Value,
        //                                                       nombreUsuario = usuario.Nombreusuario,
        //                                                       nombrepersona = Persona.Nombre + " " + Persona.Appaterno + " " + Persona.Apmaterno
        //                                                   }

        //                                                    ).ToList();
        //        return listaTipoUsuario;

        //    }
        //}


        [HttpGet]
        [Route("api/Users/validarUsuario/{idUsuario}/{nombre}")]
        public int validarUsuario(int idUsuario, string nombre)
        {
            int rpta = 0;
            try
            {

                if (idUsuario == 0)
                {
                    rpta = TodoItems.Usuarios.Where(t => t.UserName.ToLower() == nombre.ToLower()).Count();
                }
                else
                {
                    rpta = TodoItems.Usuarios.Where(t => t.UserName.ToLower() == nombre.ToLower() && t.id != idUsuario).Count();

                }

            }
            catch (Exception)
            {

                rpta = 0;
            }

            return rpta;

        }
        [HttpGet]
        [Route("api/Users/RecuperarUsuario/{idUsuario}")]
        public UsuarioViewModel RecuperarUsuario(int idUsuario)
        {

            UsuarioViewModel usarioVM = new UsuarioViewModel();
            Users usuario = TodoItems.GetUserById(idUsuario);
            usarioVM.id = usuario.id;
            usarioVM.nombreUsuario = usuario.UserName;
            //usarioVM.idtipousuario = (int)usuario.id;       //Arreglar en un momento
            return usarioVM;

        }

        [HttpGet]
        [Route("api/Users/DeleteUser/{idusuario}")]
        public int DeleteUser(int idusuario)
        {
            int respuesta = 0;

            try
            {

                Users ousuario = TodoItems.GetUserById(idusuario);
                ousuario.IsActive = false;
                TodoItems.EditUser(ousuario);

                //TodoItems.DeleteUser(idusuario);
                //bd.Entry(ousuario).State = EntityState.Modified;
                //bd.SaveChanges();
                respuesta = 1;
                AuditorSave(OperationsController.ApplicativeController_UserBaja,  OperationsDescription.ApplicativeController_UserB+ousuario.Email);

            }
            catch (Exception)
            {

                respuesta = 0;
            }

            return respuesta;



        }



        [HttpPost]
        [Route("api/Users/login")]
        public UsuarioViewModel login([FromBody]UsuarioViewModel usuario)
        {
            int respuesta = 0;
            UsuarioViewModel usuarioView = new UsuarioViewModel();


            string cifradaPassword = CifrarPassword(usuario.contra);

            respuesta = TodoItems.Usuarios.Where(t => t.UserName.ToUpper() == usuario.nombreUsuario.ToUpper() && t.Password == cifradaPassword).Count();

            if (respuesta == 1)
            {
                int usuarioe = TodoItems.Usuarios.Where(t => t.UserName == usuario.email).FirstOrDefault().id;
                string rol = TodoUserRolAplicative.ApplicativeUserRoles.Where(t => t.UserId == usuarioe).FirstOrDefault().RoleId.ToString();

                //HttpContext.Session.SetString("usuario", returnModel.userId.ToString());
                HttpContext.Session.SetString("tipoUsuario", rol); //arreglar mas adelante
                Users ouser = TodoItems.Usuarios.Where(t => t.UserName.ToUpper() == usuario.nombreUsuario.ToUpper() && t.Password == cifradaPassword).FirstOrDefault();
                HttpContext.Session.SetString("usuario", ouser.id.ToString());
                HttpContext.Session.SetString("tipoUsuario", rol); //arreglar mas adelante
                usuarioView.id = ouser.id;
                usuarioView.nombreUsuario = usuario.nombreUsuario;
              //  AuditorSave(OperationsController.UsersController_login, "http//publiclogin", OperationsDescription.UsersController_login);

            }
            else
            {
                usuarioView.id = 0;
                usuarioView.nombreUsuario = "";
            }

            return usuarioView;


        }

        [HttpGet]
        [Route("api/Users/obtenerVariableSession")]
        public SeguridadViewModel obtenerVariableSession()
        {
            SeguridadViewModel seguridad = new SeguridadViewModel();
            string variableSesion = HttpContext.Session.GetString("usuario");
            if (variableSesion == null)
            {
                seguridad.valor = "";
            }
            else
            {
                seguridad.valor = variableSesion;
                List<PermisoViewModel> lista = new List<PermisoViewModel>();
                int idusuario = int.Parse(HttpContext.Session.GetString("usuario"));
                int tipoUsuario = int.Parse(HttpContext.Session.GetString("tipoUsuario"));

                lista = (from usuario in TodoItems.Usuarios
                        // join tipousuario in TodoRol.Roles
                        //// on usuario.Iidtipousuario equals tipousuario.Id
                        // join pagitipo in TodoPaginaRol.PaginaRol on
                        //// usuario.Iidtipousuario equals pagitipo.idRol
                        // join pagina in TodoPagina.Paginas on pagitipo.Iidpagina equals pagina.Id
                        // where usuario.id == idusuario && usuario.Iidtipousuario == tipoUsuario
                         select new PermisoViewModel
                         {
                             //Accion = pagina.Accion.Substring(1)
                         }).ToList();
                seguridad.listaPagina = lista;



            }
            return seguridad;
        }
        [HttpPost]
        [Route("api/Users/SaveInformation")]
        public int SaveInformation([FromBody]UsuarioViewModel usuarioViewModel)
        {

            int respuesta = 0;
            try
            {




                if (usuarioViewModel.id == 0)
                {

      //              ,[Email]
      //,[UserName]
      //,[Name]
      //,[Surname]
      //,[IsActive]
      //,[CreationDate]
      //,[ModificationDate]
      //,[Password]


        Users ousuario = new Users();
                    ousuario.UserName = usuarioViewModel.email;// usuarioViewModel.nombreUsuario;
                    ousuario.Surname = usuarioViewModel.email;// usuarioViewModel.nombreUsuario;
                    ousuario.Name = usuarioViewModel.email;// usuarioViewModel.nombrepersona;
                    ousuario.Email = usuarioViewModel.email;
                    string claveCifrada =  CifrarPassword(usuarioViewModel.contra);
                    ousuario.Password = claveCifrada;
                    //ousuario.id = usuarioViewModel.idPersona;
                    ousuario.CreationDate = DateTime.Now;
                    ousuario.ModificationDate = DateTime.Now;
                    ousuario.IsActive = true;
                    TodoItems.AddUser(ousuario);

                    //ApplicativeUserRoles applicativeUserRoles = new ApplicativeUserRoles() { ApplicativeId = usuarioViewModel.idApplicative, RoleId = usuarioViewModel.idRol, UserId = TodoItems.Usuarios.Where(t => t.Email == usuarioViewModel.email).FirstOrDefault().id };
                    //TodoUserRolAplicative.AddApplicativeUserRol(applicativeUserRoles);
                   
                    



                    respuesta = 1;
                    AuditorSave(OperationsController.ApplicativeController_UserAlta, OperationsDescription.ApplicativeController_User+ usuarioViewModel.email);


                }
                else
                {

                    Users ousuario = TodoItems.GetUserById(usuarioViewModel.id);
                    ousuario.UserName = usuarioViewModel.email;
                    ousuario.Email = usuarioViewModel.email;
                    ousuario.CreationDate = DateTime.Now;
                    ousuario.ModificationDate = DateTime.Now;
                    // ousuario.Iidtipousuario = usuarioViewModel.idtipousuario;
                    TodoItems.EditUser(ousuario);
                   // AuditorSave(OperationsController.UsersController_SaveInformation, "http//publiclogin", "Usuario Editado");

                    respuesta = 1;


                }





            }
            catch (Exception e)
            {
               Debug.WriteLine(e.Message);
                respuesta = 0; ;
            }
            return respuesta;

        }

        private static string CifrarPassword(string contra)
        {
            SHA256Managed sHA = new SHA256Managed();
            string clave = contra;
            byte[] dataNoCifrada = Encoding.Default.GetBytes(clave);
            byte[] dataCifrada = sHA.ComputeHash(dataNoCifrada);
            string claveCifrada = BitConverter.ToString(dataCifrada).Replace("-", "");
            return claveCifrada;
        }


        


            [HttpGet]
        [Route("api/Users/getAllUserRolAplicativePage")]
        public List<ApplicativeCategoriesPagesViewModel> getAllUserRolAplicativePage()
        {
            List<ApplicativeCategoriesPagesViewModel> applicativeCategoriesPagesViewModel = new List<ApplicativeCategoriesPagesViewModel>();


            applicativeCategoriesPagesViewModel = (from aplicativeroleUser in todoItemsApplicativeRoleUserPage.ApplicativeRoleUserPages
                                                   join user in TodoItems.Usuarios on aplicativeroleUser.UserId equals user.id
                                                   join roles in TodoRol.Roles on aplicativeroleUser.RoleId equals roles.Id
                                                   join applicative in TodoItemsAplicative.Applicatives on aplicativeroleUser.AplicativeId equals applicative.Id
                                                   join permiso in TodoPermiso.Permission on aplicativeroleUser.PermissionId equals permiso.Id
                                                   join pag in TodoPages.Pages on permiso.Id equals pag.PermisoId
                                                   join categorie in TodoCategories.Categories on pag.CategoriesId equals categorie.Id
                                                   
                                                   select new ApplicativeCategoriesPagesViewModel()
                                                   {
                                                       UserId = aplicativeroleUser.UserId,
                                                       NombreRol = roles.Name,
                                                       UserName=user.Email,
                                                       RolId = roles.Id,
                                                       AplicativeName = applicative.Name,
                                                       AplicativId = applicative.Id,
                                                       PermissionId = permiso.Id,
                                                       PermissionName = permiso.Accion,
                                                       Categorieid = categorie.Id,
                                                       Categoriename = categorie.Name,
                                                       PageId = pag.Id,
                                                       PageName = pag.Name

                                                   }
                                                  ).ToList();

            return applicativeCategoriesPagesViewModel;

        }

        [HttpGet]
        [Route("api/Users/ListarPagina")]
        public List<ApplicaCategoriaPageViewModel> ListarPagina()
        {
            int idTipoUsuario, idUsuario = 0;
            List<PermisoViewModel> paginaViews = new List<PermisoViewModel>();
            List<ApplicativeCategoriesPagesViewModel> applicativeCategoriesPagesViewModel = new List<ApplicativeCategoriesPagesViewModel>();
            List<PageViewModel> ListPagesbyUser = new List<PageViewModel>();
            List<CategoriesViewModel> ListCategoriesViewmodel;
            List<ApplicaCategoriaPageViewModel> ApplicaCategoriaPageViewModel = new List<ApplicaCategoriaPageViewModel>();
            try
            {
                //idTipoUsuario = int.Parse(HttpContext.Session.GetString("tipoUsuario"));
                 idUsuario = int.Parse(HttpContext.Session.GetString("usuario"));
                //idUsuario = 4;
            }
            catch (Exception e)
            {

                return ApplicaCategoriaPageViewModel;
            }


            applicativeCategoriesPagesViewModel = (from aplicativeroleUser in todoItemsApplicativeRoleUserPage.ApplicativeRoleUserPages
                                                   join roles in TodoRol.Roles on aplicativeroleUser.RoleId equals roles.Id
                                                   join applicative in TodoItemsAplicative.Applicatives on aplicativeroleUser.AplicativeId equals applicative.Id
                                                   join permiso in TodoPermiso.Permission on aplicativeroleUser.PermissionId equals permiso.Id
                                                   join pag in TodoPages.Pages on permiso.Id equals pag.PermisoId
                                                   join categorie in TodoCategories.Categories on pag.CategoriesId equals categorie.Id
                                                   where aplicativeroleUser.UserId == idUsuario
                                                   select new ApplicativeCategoriesPagesViewModel() { 
                                                   UserId= aplicativeroleUser.UserId,
                                                   NombreRol=roles.Name,
                                                   RolId=roles.Id,
                                                   AplicativeName=applicative.Name,
                                                   AplicativId=applicative.Id,
                                                   PermissionId=permiso.Id,
                                                   PermissionName=permiso.Accion,
                                                   Categorieid= categorie.Id,
                                                   Categoriename=categorie.Name,
                                                   PageId=pag.Id,
                                                   PageName=pag.Name

                                                   }
                                                   ).ToList();




            var cantApplicativodiferentes = applicativeCategoriesPagesViewModel.Select(t => t.AplicativeName).Distinct().ToList();

            for (int i = 0; i < cantApplicativodiferentes.Count; i++)
            {

                List<int> listadocategorias = applicativeCategoriesPagesViewModel.Where(t => t.AplicativeName == cantApplicativodiferentes[i]).Select(t=>t.Categorieid).Distinct().ToList();
                ListCategoriesViewmodel = new List<CategoriesViewModel>();
                for (int j = 0; j < listadocategorias.Count; j++)
                {
                    ListPagesbyUser = new List<PageViewModel>();
                    var todaslaspaginasCategorias = TodoPages.Pages.Where(t => t.CategoriesId == listadocategorias[j]).ToList();
                    for (int k = 0; k< todaslaspaginasCategorias.Count; k++)
                    {
                        if (applicativeCategoriesPagesViewModel.Where(t=>t.PageId== todaslaspaginasCategorias[k].Id).Count()>0)
                        {

                            PageViewModel pageView = new PageViewModel();
                            pageView.Name = todaslaspaginasCategorias[k].Name;
                            pageView.namePermiso = TodoPermiso.Permission.Where(t => t.Id == todaslaspaginasCategorias[k].PermisoId).FirstOrDefault().Accion;
                            pageView.isActive = todaslaspaginasCategorias[k].IsActive;
                            pageView.PermisoId = todaslaspaginasCategorias[k].PermisoId;
                            pageView.Id = todaslaspaginasCategorias[k].Id;
                            pageView.CategoriesId = todaslaspaginasCategorias[k].CategoriesId;
                            pageView.Description = todaslaspaginasCategorias[k].Description;
                           


                            ListPagesbyUser.Add(pageView);
                        }
                    }

                   
                    ListCategoriesViewmodel.Add(new CategoriesViewModel() { 
                    Name=TodoCategories.Categories.Where(t=>t.Id== listadocategorias[j]).FirstOrDefault().Name,
                    ListadoPagina= ListPagesbyUser
                    });


                   
                }

                ApplicaCategoriaPageViewModel.Add(new ApplicaCategoriaPageViewModel()
                {
                    name = cantApplicativodiferentes[i],
                    ListCategories = ListCategoriesViewmodel
                });


            }


            //var apllicativeRolbyuser = todoItemsApplicativeRoleUserPage.ApplicativeRoleUserPages.Where(t => t.UserId == idUsuario).ToList();

            //foreach (var item in apllicativeRolbyuser)
            //{
            //    ListPagesbyUser.Add(TodoPages.Pages.FirstOrDefault(t => t.PermisoId == item.PermissionId));
            //}


            //var queryLastNames = from listado in ListPagesbyUser
            //                     group listado by listado.CategoriesId into newGroup

            //                     select newGroup;

            //List<Categorie> listcategoriesByApplicative = new List<Categorie>();
            //foreach (var query in queryLastNames)
            //{

            //    DictionaryPageviewModel dictionary = new DictionaryPageviewModel();


            //    listcategoriesByApplicative.Add(TodoCategories.Categories.Where(t => t.Id == query.Key).FirstOrDefault());
            //    //foreach (var querys in query)
            //    //{




            //    //}

            //    //dictionaryPageviewModels.Add(new DictionaryPageviewModel() { Name = query.Key, PageListAsociateteName = new List<PageViewModel>() { query[query.Key] } })
            //    // Console.WriteLine($"\t{student.LastName}, {student.FirstName}");
            //}





            //List<Applicatives> listApplicative = new List<Applicatives>();
            //List<UserRol> listrols = new List<UserRol>();
            //List<Categorie> listcategoriesByApplicative;
            //Dictionary<int, List<Categorie>> diccionarioCategoriasByApplicative = new Dictionary<int, List<Categorie>>();


            //listApplicative = (from aplicativeuserrol in TodoUserRolAplicative.ApplicativeUserRoles
            //                join aplicativert in TodoItemsAplicative.Applicatives
            //                on aplicativeuserrol.ApplicativeId equals aplicativert.Id
            //                where aplicativeuserrol.UserId == idUsuario
            //                select aplicativert).ToList();


            //listrols = (from aplicativeuserrol in TodoUserRolAplicative.ApplicativeUserRoles
            //                   join roles in TodoRol.Roles
            //                   on aplicativeuserrol.RoleId equals roles.Id
            //                   where aplicativeuserrol.UserId == idUsuario
            //                   select roles).ToList();



            //foreach (var item in listApplicative)
            //{
            //    listcategoriesByApplicative = new List<Categorie>();
            //    listcategoriesByApplicative = TodoCategories.Categories.Where(t => t.ApplicationId == item.Id).ToList();

            //    diccionarioCategoriasByApplicative.Add(item.Id, listcategoriesByApplicative);
            //}






            // TodoUserRolAplicative.ApplicativeUserRoles.Where(t => t.UserId == idUsuario).Select(t=>t).ToList();

            //paginaViews = (from permisorol in TodoPermisoRol.PermisoRol
            //               join permiso in TodoPermiso.Permission
            //               on permisorol.IdPage equals permiso.Id
            //               where permisorol.IsActive == true && permisorol.IdRol == idTipoUsuario &&
            //               permiso.IsVisible == true
            //               select new PermisoViewModel
            //               {
            //                   Id = permiso.Id,
            //                   IsActive = permiso.IsActive,
            //                   Accion = permiso.Accion,
            //                   Message = permiso.Message,
            //                   IsVisible=permiso.IsVisible



            //               }).ToList();
            //auditorController.AuditorSave(OperationsController.UsersController_ListarPagina, "http//publiclogin", OperationsDescription.UsersController_ListarPagina);

            return ApplicaCategoriaPageViewModel;



        }




        [HttpGet]
        [Route("api/Users/CerrarSesion")]
        public SeguridadViewModel CerrarSesion()
        {
            SeguridadViewModel seguridad = new SeguridadViewModel();
            try
            {

                HttpContext.Session.Remove("usuario");
                HttpContext.Session.Remove("tipoUsuario");
                seguridad.valor = "OK";
            }
            catch (Exception)
            {

                seguridad.valor = "";
            }
            return seguridad;

        }


        public void AuditorSave(string Consult,  string Description)
        {
            try
            {
                int tipoUsuario, idusuario = 0;

                if (HttpContext == null)
                {
                    //    if (string.IsNullOrEmpty(HttpContext.Session.GetString("usuario")))
                    //{
                    idusuario = 11;
                    tipoUsuario = 5;
                }
                else
                {
                    idusuario = int.Parse(HttpContext.Session.GetString("usuario"));
                   // tipoUsuario = int.Parse(HttpContext.Session.GetString("tipoUsuario"));

                }
                //if (string.IsNullOrEmpty(HttpContext.Session.GetString("tipoUsuario")))
                //{
                //    tipoUsuario = 99999;
                //}
                //else {

                //     tipoUsuario = int.Parse(HttpContext.Session.GetString("tipoUsuario"));

                //}


                DateTime CreationDate = DateTime.Now;

                Auditor auditor = new Auditor();
                auditor.Consult = Consult;
                auditor.CreationDate = CreationDate;
                auditor.Description = Description;               
                auditor.UserId = idusuario;
                auditor.Url = "http://soportol";
                todoItemsAuditor.AddAuditor(auditor);

            }
            catch (Exception)
            {

                throw;
            }
            //,[Consult]
            //,[Url]
            //,[Description]

        }





    }
}