﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ToolsSoport.Models.Paginas
{
    [Table("Pages")]
    public class Page
    {



        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
      public int CategoriesId { get; set; }
        public int PermisoId { get; set; }
            


        public bool IsActive { get; set; }
        public DateTime CreationDate { get; set; }
      public DateTime ModificationDate { get; set; }
    }
}
