﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ToolsSoport.Models.Paginas;
using ToolsSoport.Models.Permiso;

namespace ToolsSoport.Controllers
{
    
    public class PermissionController : ControllerBase
    {


        public IPermisoRepository TodoItems { get; set; }
      

        public PermissionController(IPermisoRepository paginaRepository)
        {
            TodoItems = paginaRepository;
        }

        [HttpGet]
        [Route("api/Permission/permissionList")]
        public IEnumerable<PermisoViewModel> permissionList()
        {


            List<PermisoViewModel> lista = (from pagina in TodoItems.Permission

                                           where pagina.IsActive == true
                                           select new PermisoViewModel
                                           {
                                               Id = pagina.Id,
                                               Accion = pagina.Accion,
                                               Message = pagina.Message,


                                           }).ToList();
            return lista;


        }


        [HttpGet]
        [Route("api/Permission/DeletePermission/{idpermiso}")]
        public int DeletePermission(int idpermiso)
        {
            int respuesta = 0;

            try
            {

                Permisos pagina = TodoItems.GetPermissionById(idpermiso); ;
                pagina.IsActive = false;
                TodoItems.EditPermission(pagina);
                respuesta = 1;

            }
            catch (Exception)
            {

                respuesta = 0;
            }

            return respuesta;
        }




        [HttpPost]
        [Route("api/Permission/SavePermission")]

        public int SavePermission([FromBody]PermisoViewModel pagina)
        {
            int respuesta = 0;

            try
            {

                if (pagina.Id == 0)
                {
                    Permisos pagina1 = new Permisos();
                    pagina1.Accion = pagina.Accion;
                    pagina1.Message = pagina.Message;
                    pagina1.IsActive = true;
                    pagina1.IsVisible = pagina.IsVisible;
                    TodoItems.AddPermission(pagina1);
                    respuesta = 1;

                }
                else
                {

                    Permisos pagina1 = TodoItems.GetPermissionById(pagina.Id);
                    pagina1.Accion = pagina.Accion;
                    pagina1.Message = pagina. Message;
                    pagina1.IsActive = pagina.IsActive;
                    pagina1.IsVisible = pagina.IsVisible;
                    TodoItems.EditPermission(pagina1);


                    respuesta = 1;

                }


            }
            catch (Exception)
            {

                respuesta = 0;
            }
            return respuesta;

        }




        [HttpGet]
        [Route("api/Permission/GetPermission/{idPermission}")]
        public PermisoViewModel GetPermission(int idPermission)
        {
            PermisoViewModel pagina = new PermisoViewModel();

            try
            {
                Permisos paginaObject = TodoItems.GetPermissionById(idPermission);
                pagina.Id = paginaObject.Id;
                pagina.Message = paginaObject.Message;
                pagina.Accion = paginaObject.Accion;
                pagina.IsVisible = paginaObject.IsVisible;
            }
            catch (Exception)
            {
                pagina.Accion = null;
            }

            return pagina;

        }


    }
}