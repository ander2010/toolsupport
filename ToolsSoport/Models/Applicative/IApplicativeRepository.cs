﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ToolsSoport.Models.Applicative
{
    public interface IApplicativeRepository
    {

        IEnumerable<Applicatives> Applicatives { get; }

        void AddApplicatives(Applicatives e);

        void EditApplicatives(Applicatives e);

        void DeleteApplicatives(int ApplicativeID);
        Applicatives GetApplicativesById(int ApplicativeID);
    }
}
