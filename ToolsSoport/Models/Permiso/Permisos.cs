﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using ToolsSoport.Models.PersonaRol;

namespace ToolsSoport.Models.Permiso
{
    [Table(name: "Permisos")]
    public class Permisos
    {
        //public Permisos()
        //{
        //    PermisosRols = new HashSet<PermisosRol>();
        //}

        public int Id { get; set; }
        public string Message { get; set; }
        public string Accion { get; set; }
        public bool IsActive { get; set; }
        public bool IsVisible { get; set; }

        //public ICollection<PermisosRol> PermisosRols { get; set; }
    }
}
