import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AuditorFormsAddeditComponent } from './auditor-forms-addedit.component';

describe('AuditorFormsAddeditComponent', () => {
  let component: AuditorFormsAddeditComponent;
  let fixture: ComponentFixture<AuditorFormsAddeditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AuditorFormsAddeditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuditorFormsAddeditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
