﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ToolsSoport.Models.Permiso
{
    public class PermisoRepository : IPermisoRepository
    {
        ToolSoportContext context;
        public PermisoRepository(ToolSoportContext ctx)
        {
            context = ctx;
        }
       

        public IEnumerable<Permisos> Permission => context.Permisos;

      

        public void AddPermission(Permisos e)
        {
            context.Permisos.Add(e);
            context.SaveChanges();
        }

        

        public void DeletePermission(int paginaID)
        {
            Permisos e = new Permisos() { Id = paginaID };
            context.Permisos.Remove(e);
            context.SaveChanges();
        }

     

        public void EditPermission(Permisos e)
        {
            Permisos dbEntry = context.Permisos.FirstOrDefault(o => o.Id == e.Id);
            if (dbEntry != null)
            {
                dbEntry.Message = e.Message;
               // dbEntry.PermisosRols = e.PermisosRols;
                dbEntry.IsVisible = e.IsVisible;
                dbEntry.IsActive = e.IsActive;
                dbEntry.Accion = e.Accion;

            }
            context.SaveChanges();
        }



        public Permisos GetPermissionById(int permisoID)
        {
            Permisos e = context.Permisos.FirstOrDefault(t => t.Id == permisoID);
            return e;
        }
    }
}
