﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ToolsSoport.Models.ApplicativeRoleUserPages
{

    [Table("ApplicativeRoleUserPage")]
    public class ApplicativeRoleUserPage
    {
      public   int Id { get; set; }
        public int UserId { get; set; }
        public int RoleId { get; set; }

        public int AplicativeId { get; set; }

        public int PermissionId { get; set; }
    



    }
}
