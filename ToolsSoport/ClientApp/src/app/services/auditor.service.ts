import { Injectable, Inject } from '@angular/core';
import { Http } from '@angular/http';
import { Router } from '@angular/router';

import 'rxjs/add/operator/map';

@Injectable()
export class AuditorService {

  urlBase: string = "";
  constructor(private http: Http, @Inject("BASE_URL") urlbase: string, private route: Router) {
    this.urlBase = urlbase
  }




  public guardarDatos(usuarioViewModel) {

    return this.http.post(this.urlBase + "api/Users/GuardarDatos", usuarioViewModel).map(t => t.json());

  }


 


  public AuditoryFilter(idauditoria) {

  return this.http.get(this.urlBase + "api/Auditor/AuditoryFilter/" + idauditoria).map(t => t.json());

}

  public EliminarAuditoria(idauditoria) {

    return this.http.get(this.urlBase + "api/Auditor/EliminarAuditoria/" + idauditoria).map(t => t.json());

  }
  public GetAuditoryType() {
    return this.http.get(this.urlBase + "api/Auditor/GetAuditoryType").map(t =>
      t.json()

    );
  }


  
  public listarAuditoria() {
    return this.http.get(this.urlBase + "api/Auditor/listarAuditoria").map(t => 
      t.json()

    );
  }



}
