import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { HttpModule } from '@angular/http';
import { UsuarioService } from '../../src/app/services/usuario.service';
import { AuditorService } from '../../src/app/services/auditor.service';
import { ApplicationService } from '../../src/app/services/application.service';
import { AppComponent } from './app.component';
import { NavMenuComponent } from './nav-menu/nav-menu.component';
import { HomeComponent } from './home/home.component';
import { CounterComponent } from './counter/counter.component';

import { FetchDataComponent } from './fetch-data/fetch-data.component';
import { ListRolsComponent } from './component/Rols/list-rols/list-rols.component';
import { MaintenanceRolsComponent } from './component/Rols/maintenance-rols/maintenance-rols.component';
import { RolsFormsAddeditComponent } from './component/Rols/rols-forms-addedit/rols-forms-addedit.component';
import { ListPageComponent } from './component/Pages/list-page/list-page.component';
import { MaintenancePageComponent } from './component/Pages/maintenance-page/maintenance-page.component';
import { PageFormsAddeditComponent } from './component/Pages/page-forms-addedit/page-forms-addedit.component';
import { ListUserComponent } from './component/Users/list-user/list-user.component';
import { MaintanceUserComponent } from './component/Users/maintance-user/maintance-user.component';
import { UserFormsAddeditComponent } from './component/Users/user-forms-addedit/user-forms-addedit.component';
import { ListAuditorComponent } from './component/Auditor/list-auditor/list-auditor.component';
import { MaintenanceAuditorComponent } from './component/Auditor/maintenance-auditor/maintenance-auditor.component';
import { AuditorFormsAddeditComponent } from './component/Auditor/auditor-forms-addedit/auditor-forms-addedit.component';
import { ApplicationFormsAddeditComponent } from './component/Application/application-forms-addedit/application-forms-addedit.component';
import { MaintenanceApplicationComponent } from './component/Application/maintenance-application/maintenance-application.component';
import { ListApplicationComponent } from './component/Application/list-application/list-application.component';
import { ApplicationsonFormsAddEdditComponent } from './component/Application/applicationson-forms-add-eddit/applicationson-forms-add-eddit.component';
import { LoginComponent } from './component/login/login.component';
import { StorageService } from './services/storage.service';
import { SecurityService } from './services/security.service';
import { AuthenticationService } from './services/authentication.service';
import { ListPermissionComponent } from './component/Permission/list-permission/list-permission.component';
import { MaintenancePermissionComponent } from './component/Permission/maintenance-permission/maintenance-permission.component';
import { PermissionFormsAddeditComponent } from './component/Permission/permission-forms-addedit/permission-forms-addedit.component';
import { EmpresaListComponent } from './component/Pages/Pagina/empresa-list/empresa-list.component';
import { EmpresaMaintainceComponent } from './component/Pages/Pagina/empresa-maintaince/empresa-maintaince.component';
import { GrantorinformatioMaintainceComponent } from './component/Pages/Pagina/grantorinformatio-maintaince/grantorinformatio-maintaince.component';
import { GrantorinformatioListComponent } from './component/Pages/Pagina/grantorinformatio-list/grantorinformatio-list.component';
import { LogListComponent } from './component/Pages/Pagina/log-list/log-list.component';
import { LogMaintanceComponent } from './component/Pages/Pagina/log-maintance/log-maintance.component';
import { CompanyCdpComponent } from './component/Empresas/CompanyCDP/company-cdp/company-cdp.component';
import { DocumentsListComponent } from './component/Documentos/documents-list/documents-list.component';
import { NotificationListComponent } from './component/Notificaciones/notification-list/notification-list.component';
import { LogscdcListComponent } from './component/Logs/LogsCDC/logscdc-list/logscdc-list.component';
import { LogscdpListComponent } from './component/Logs/LogsCDP/logscdp-list/logscdp-list.component';
import { WebhookscdcListComponent } from './component/Webhooks/WebhooksCDC/webhookscdc-list/webhookscdc-list.component';
import { WebhookscdpListComponent } from './component/Webhooks/WebhooksCDP/webhookscdp-list/webhookscdp-list.component';
import { SeguridadListComponent } from './component/Seguridad/seguridad-list/seguridad-list.component';
import { MaintanceSeguridadComponent } from './component/Seguridad/maintance-seguridad/maintance-seguridad.component';
import { SeguridadFormsAddeditComponent } from './component/Seguridad/seguridad-forms-addedit/seguridad-forms-addedit.component';
import { InvitadosComponent } from './component/invitados/invitados.component';


@NgModule({
  declarations: [
    AppComponent,
    NavMenuComponent,
    HomeComponent,
    CounterComponent,
    FetchDataComponent,
    ListRolsComponent,
    MaintenanceRolsComponent,
    RolsFormsAddeditComponent,
    ListPageComponent,
    MaintenancePageComponent,
    PageFormsAddeditComponent,
    ListUserComponent,
    MaintanceUserComponent,
    UserFormsAddeditComponent,
    ListAuditorComponent,
    MaintenanceAuditorComponent,
    AuditorFormsAddeditComponent,
    ApplicationFormsAddeditComponent,
    MaintenanceApplicationComponent,
    ListApplicationComponent,
    ApplicationsonFormsAddEdditComponent,
    LoginComponent,
    ListPermissionComponent,
    MaintenancePermissionComponent,
    PermissionFormsAddeditComponent,
    EmpresaListComponent,
    EmpresaMaintainceComponent,
    GrantorinformatioMaintainceComponent,
    GrantorinformatioListComponent,
    LogListComponent,
    LogMaintanceComponent,
    CompanyCdpComponent,
    DocumentsListComponent,
    NotificationListComponent,
    LogscdcListComponent,
    LogscdpListComponent,
    WebhookscdcListComponent,
    WebhookscdpListComponent,
    SeguridadListComponent,
    MaintanceSeguridadComponent,
    SeguridadFormsAddeditComponent,
    InvitadosComponent,
   
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    
  HttpModule,
    HttpClientModule,
    FormsModule, ReactiveFormsModule,
    RouterModule.forRoot([
      { path: '', component: HomeComponent, pathMatch: 'full' },
      { path: 'home', component: HomeComponent },
      { path: 'counter', component: CounterComponent },
      { path: 'fetch-data', component: FetchDataComponent },
      { path: 'Rols', component: MaintenanceRolsComponent },
      { path: 'roles-addEdit/:id', component: RolsFormsAddeditComponent },
      { path: 'Pages', component: MaintenancePageComponent },
      { path: 'Page-addEdit/:id', component: PageFormsAddeditComponent },
      { path: 'Users', component: MaintanceUserComponent },
      { path: 'Users-addEdit/:id', component: UserFormsAddeditComponent },
      
       { path: 'login', component: LoginComponent },

      { path: 'Application', component: MaintenanceApplicationComponent },
      { path: 'Application-addEdit/:id', component: ApplicationFormsAddeditComponent },
      { path: 'ApplicationSon-addEdit/:id', component: ApplicationsonFormsAddEdditComponent },
      { path: 'Auditor', component: MaintenanceAuditorComponent },
      { path: 'Auditor-addEdit/:id', component: AuditorFormsAddeditComponent },


      { path: 'Permission', component: MaintenancePermissionComponent },
      { path: 'Permission-addEdit/:id', component: PermissionFormsAddeditComponent },


      { path: 'Grantorinformation', component: GrantorinformatioMaintainceComponent },
      { path: 'Empresa_CDP', component: EmpresaMaintainceComponent },
      { path: 'Log', component: LogMaintanceComponent },

      { path: 'Empresa_CDC', component: EmpresaMaintainceComponent },
      { path: 'Log_CDC', component: LogscdcListComponent },


      { path: 'WebHooks_CDP', component: WebhookscdpListComponent },
      { path: 'WebHooks_CDC', component: WebhookscdcListComponent },
      { path: 'Log_CDP', component: LogscdpListComponent },

      { path: 'Seguridad', component: MaintanceSeguridadComponent },
      { path: 'Seguridad-addEdit/:id', component: SeguridadFormsAddeditComponent },
      { path: 'Notificaciones', component: NotificationListComponent },
      { path: 'Documentos', component: DocumentsListComponent },
      { path: 'Invitado', component: InvitadosComponent },
      

      

      

      
    ])
  ],
  providers: [UsuarioService, AuditorService, ApplicationService, StorageService, AuthenticationService, SecurityService],
  bootstrap: [AppComponent]
})
export class AppModule { }
