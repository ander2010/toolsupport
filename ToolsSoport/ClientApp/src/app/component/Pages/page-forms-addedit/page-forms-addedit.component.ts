import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UsuarioService } from '../../../services/usuario.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-page-forms-addedit',
  templateUrl: './page-forms-addedit.component.html',
  styleUrls: ['./page-forms-addedit.component.css']
})
export class PageFormsAddeditComponent implements OnInit {

  pagina: FormGroup;
  titulo: string;
  parametro: string;
  constructor(private usuarioSErvice: UsuarioService, private ruta: Router, private activateroute: ActivatedRoute) {
    this.pagina = new FormGroup({
      "id": new FormControl("0"),
      "message": new FormControl("", [Validators.required, Validators.maxLength(100)]),
      "accion": new FormControl("", [Validators.required, Validators.maxLength(100)]),
      "isActive": new FormControl("1", Validators.required),
      "isVisible": new FormControl("1"),


    });

    this.activateroute.params.subscribe(parametro => {
      this.parametro = parametro["id"];
      if (this.parametro == "id") {
        this.titulo = "Agregando una nueva Pagina"

      }
      else {

        this.titulo = "Editando una Pagina";


      }
    });
  }

  ngOnInit() {


    if (this.parametro == "id") {
      this.titulo = "Agregando una nueva Pagina"



    }
    else {

      this.usuarioSErvice.RecuperarPagina(this.parametro).subscribe(t => {

        if (t.accion == null) {
          this.ruta.navigate(['/no-encontro-informacion']);
        } else {
          this.pagina.controls["id"].setValue(t.id);
          this.pagina.controls["message"].setValue(t.message);
          this.pagina.controls["accion"].setValue(t.accion);
          this.pagina.controls["isVisible"].setValue(t.isVisible.toString());
        }
        //this.persona.controls["apMaterno"].setValue(t.apMaterno);
        //this.persona.controls["telefono"].setValue(t.telefono);
        //this.persona.controls["correo"].setValue(t.correo);
        //this.persona.controls["fechanacimiento"].setValue(t.fechaCadena);

      });


    }


  }
  guardarDatos() {
    console.info(this.pagina.value);
    this.usuarioSErvice.GuardarPagina(this.pagina.value).subscribe(t => { this.ruta.navigate(['/Pages']) });
  }

}



//<!--public int Id { get; set; }
//        public string Message { get; set; }
//        public string Accion { get; set; }
//        public int ? IsActive { get; set; }
//        public int ? IsVisible { get; set; } -->
