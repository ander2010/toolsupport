import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { UsuarioService } from '../../../services/usuario.service';
import { ApplicationService } from '../../../services/application.service';

@Component({
  selector: 'app-user-forms-addedit',
  templateUrl: './user-forms-addedit.component.html',
  styleUrls: ['./user-forms-addedit.component.css']
})
export class UserFormsAddeditComponent implements OnInit {
  usuario: FormGroup;
  tipoUsuarios: any;
  Applicativos: any;
  parametro: string = "";
  titulo: string = "";
  ver: boolean = true;
  constructor(private UsuarioService: UsuarioService, private applicationservice: ApplicationService,
    private activateRoute: ActivatedRoute, private router: Router) {
    this.usuario = new FormGroup({
      "id": new FormControl("0"),
      "email": new FormControl("", [Validators.required, Validators.maxLength(100)], this.noRepetirUsuariosInsertar.bind(this)),
      "idPersona": new FormControl("0"),
      "contra": new FormControl("", [Validators.required, Validators.maxLength(100)]),
      "contra2": new FormControl("", [Validators.required, Validators.maxLength(100), this.validarContraIguales.bind(this)]),
      //"idApplicative": new FormControl("", [Validators.required, Validators.maxLength(100)]),
      //"idRol": new FormControl("", [Validators.required, Validators.maxLength(100)]),


    });


    this.activateRoute.params.subscribe(t => {

      this.parametro = t["id"];

      if (this.parametro == "id") {
        this.ver = true;
      } else {
        this.ver = false;
        this.UsuarioService.RecuperarUsuario(this.parametro).subscribe(t => {
          this.usuario.controls["id"].setValue(t.id);
          this.usuario.controls["email"].setValue(t.nombreUsuario);
          //this.usuario.controls["idtipousuario"].setValue(t.idtipousuario);
        });
        this.usuario.controls["contra"].setValue("1");
        this.usuario.controls["contra2"].setValue("1");
        //this.usuario.controls["idApplicative"].setValue("1");
        //this.usuario.controls["idRol"].setValue("1");
      }
    });

    if (this.parametro == "id") {
      this.titulo = "Agregando un nuevo Usuario";
    } else {

      this.titulo = "Editando un  usuario";

    }
  }

  ngOnInit() {

    //this.UsuarioService.gettipoUsuario().subscribe(t => this.tipoUsuarios = t);
    //this.applicationservice.listarApplicative().subscribe(t => this.Applicativos = t);


  }
  guardarDatos() {

    if (this.usuario.valid == true) {
      this.UsuarioService.guardarDatos(this.usuario.value).subscribe(t => {

        this.router.navigate(["/Users"]);
      });
    }

  }

  validarContraIguales(control: FormControl) {

    if (control.value != "" && control.value != null) {
      if (this.usuario.controls["contra"].value != control.value) {
        return { noIguales: true }
      } else {
        return null;
      }
    }
  }


  noRepetirUsuariosInsertar(control: FormControl) {
    var promesa = new Promise((resolve, reject) => {
      if (control.value != "" && control.value != null) {
        this.UsuarioService.validarUsuario(this.usuario.controls["id"].value, control.value).subscribe(t => {

          if (t == 1) {
            resolve({ yaExiste: true })
          } else {

            resolve(null);
          }
        });
      }

    });
    return promesa;

  }

}
