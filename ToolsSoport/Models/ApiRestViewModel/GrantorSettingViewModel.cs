﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ToolsSoport.Models.ApiRestViewModel
{
    public class GrantorSettingViewModel
    {

        public Guid Id { get; set; }
        public string Status { get; set; }
        public string TaxId { get; set; }

        public string BusinessName { get; set; }

        public string ShortName { get; set; }

        public string BusinessType { get; set; }

        public string Name1 { get; set; }

        public string Name2 { get; set; }

        public string Surname1 { get; set; }

        public string Surname2 { get; set; }

        public string Application { get; set; }


        


    }
}