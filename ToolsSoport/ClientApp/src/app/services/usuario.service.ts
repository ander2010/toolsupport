import { Injectable, Inject } from '@angular/core';
import { Http } from '@angular/http';
import { Router } from '@angular/router';

import 'rxjs/add/operator/map';

@Injectable()
export class UsuarioService {

  urlBase: string = "";
  constructor(private http: Http, @Inject("BASE_URL") urlbase: string, private route: Router) {
    this.urlBase = urlbase
  }

  public getUsuario() {
    return this.http.get(this.urlBase + "api/Users/UserList").map(t => t.json());
  }

  public gettipoUsuario() {

    return this.http.get(this.urlBase + "api/Users/RoleList").map(t => t.json());

  }
  public getFiltrarUsuarioTipo(idtipo) {

    return this.http.get(this.urlBase + "api/Users/FiltradoUsuariotipo/" + idtipo).map(t => t.json());

  }
  public validarUsuario(idusuario, nombre) {
    return this.http.get(this.urlBase + "api/Users/validarUsuario/" + idusuario + "/" + nombre).map(t => t.json());


  }

  public RecuperarUsuario(idUsuario) {

    return this.http.get(this.urlBase + "api/Users/RecuperarUsuario/" + idUsuario).map(t => t.json());
  }

  public guardarDatos(usuarioViewModel) {

    return this.http.post(this.urlBase + "api/Users/SaveInformation", usuarioViewModel).map(t => t.json());

  }

  public guardarDatoscompeltos(usuarioViewModel) {

 // return this.http.post(this.urlBase + "api/Users/SaveInformation", usuarioViewModel).map(t => t.json());

  }

  public eliminarUsuario(idusuario) {

    return this.http.get(this.urlBase + "api/Users/DeleteUser/" + idusuario).map(t => t.json());

  }

  public login(Usuario) {

    return this.http.post(this.urlBase + "api/Users/login", Usuario).map(t => t.json());


  }
  public obtenerVariableSession(next) {

    return this.http.get(this.urlBase + "api/Users/obtenerVariableSession").map(t => {
      var data = t.json();
      var informacion = data.valor;
      if (informacion == "") {

        this.route.navigate(['/pagina-error']);
        return false;

      } else {

        var pagia = next["url"][0].path;
        if (data.listaPagina != null) {


          var paginas = data.listaPagina.map(t => t.accion);
          if (paginas.indexOf(pagia) > -1 && pagia != "Login") {
            return true;
          } else {
            this.route.navigate(["/pagina-error-permiso"]);
            return false;
          }
        }
        return true;

      }

    });
  }

  public CerrarSesion() {

    return this.http.get(this.urlBase + "api/Users/CerrarSesion").map(t => t.json());
  }

  public obtenerSession() {

    return this.http.get(this.urlBase + "api/Users/obtenerVariableSession").map(t => {
      var data = t.json();
      var informacion = data.valor;
      if (informacion == "") {

        return false;

      } else {
        return true;
      }

    });
  }
  public ListarPagina() {

    return this.http.get(this.urlBase + "api/Users/ListarPagina").map(t => t.json());

  }

  public ListarTipoUsuario() {

    return this.http.get(this.urlBase + "api/Rol/ListarTipoUsuario").map(t => t.json());
  }

  public listarPaginastipoUsuario() {
    return this.http.get(this.urlBase + "api/Rol/listarPaginastipoUsuario").map(t => t.json());

  }

  public ListarpaginaRecuperadas(idtipoUsuario) {

    return this.http.get(this.urlBase + "api/Rol/ListarpaginaRecuperadas/" + idtipoUsuario).map(t => t.json());
  }

  public eliminarTipoUsuario(idtipoUsuario) {

    return this.http.get(this.urlBase + "api/Rol/eliminarTipoUsuario/" + idtipoUsuario).map(t => t.json());
  }




  public guardarDatosTipoUsuario(tipousuarioViewModel) {

    return this.http.post(this.urlBase + "api/Rol/guardarDatosTipoUsuario", tipousuarioViewModel).map(t => t.json());

  }

  public listarpaginas() {
    return this.http.get(this.urlBase + "api/Permission/permissionList").map(t => t.json());
  }


  public GuardarPagina(Pagina) {

    return this.http.post(this.urlBase + "api/Permission/SavePermission", Pagina).map(t => t.json());

  }

  public EliminarPagina(idpagina) {
    return this.http.get(this.urlBase + "api/Permission/DeletePermission/" + idpagina).map(t => t.json());
  }

  public RecuperarPagina(idpagina) {
    return this.http.get(this.urlBase + "pi/Permission/GetPermission/" + idpagina).map(t => t.json());
  }

}
