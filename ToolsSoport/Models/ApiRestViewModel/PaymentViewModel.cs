﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ToolsSoport.Models.ApiRestViewModel
{
    public class PaymentViewModel
    {


        public DateTime PaymentDate { get; set; }
        public Guid documentId { get; set; }
        public decimal Amount { get; set; }
        public string Currency { get; set; }
        public string PaymentMethod { get; set; }
             public string Type { get; set; }
                                                                      
             public bool? isLoadedByApi { get; set; }
                public Guid? PaymentTransactionId { get; set; }
                public string Comments { get; set; }




    }
}