import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MaintenanceAuditorComponent } from './maintenance-auditor.component';

describe('MaintenanceAuditorComponent', () => {
  let component: MaintenanceAuditorComponent;
  let fixture: ComponentFixture<MaintenanceAuditorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MaintenanceAuditorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MaintenanceAuditorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
