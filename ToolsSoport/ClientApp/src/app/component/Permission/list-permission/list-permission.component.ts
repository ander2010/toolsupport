import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { UsuarioService } from '../../../services/usuario.service';

@Component({
  selector: 'list-permission',
  templateUrl: './list-permission.component.html',
  styleUrls: ['./list-permission.component.css']
})
export class ListPermissionComponent implements OnInit {
  @Input() permissions: any;
  @Input() isMantenimiento = false;
  cabeceras: string[] = ["Id pagina", "Mensaje", "Accion"]
  constructor(private usuarioservice: UsuarioService, private ruta: Router, private activateroute: ActivatedRoute) { }

  ngOnInit() {
    this.usuarioservice.listarpaginas().subscribe(t => this.permissions = t);
    console.info(this.permissions);
  }


  eliminarpagina(id) {

    this.usuarioservice.EliminarPagina(id).subscribe(t => {

      this.usuarioservice.listarpaginas().subscribe(t => this.permissions = t);
    });

  }



}
