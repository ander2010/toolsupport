﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ToolsSoport.Helpers;
using ToolsSoport.Models.ApiRestViewModel;
using ToolsSoport.Models.Auditors;
using ToolsSoport.Models.Usuarios;

namespace ToolsSoport.Controllers
{
    
    public class AuditorController : ControllerBase
    {

        
         IAuditorRepository todoItemsAuditor { get; set; }
        private IUsuarioRepository TodoItems { get; set; }
        //IPaginaRolRepository todoItemPaginaRol { get; set; }

        public AuditorController(IAuditorRepository todoItemsAuditores, IUsuarioRepository TodoItems)
        {
            todoItemsAuditor = todoItemsAuditores;
            this.TodoItems = TodoItems;
        }




        [HttpGet]
        [Route("api/Auditor/AuditoryFilter/{idAuditoria}")]
        public IEnumerable<AuditorViewModel> AuditoryFilter(string idAuditoria)
        {
            List<AuditorViewModel> lista = (from auditoria in todoItemsAuditor.AuditorLog
                                            where auditoria.Consult.ToUpper() ==idAuditoria.ToUpper()

                                            select new AuditorViewModel
                                            {
                                                Id = auditoria.Id,
                                                Consult = auditoria.Consult,
                                                CreationDate = auditoria.CreationDate,
                                                email = string.IsNullOrEmpty(TodoItems.Usuarios.FirstOrDefault(t => t.id == auditoria.UserId).Email) ? "usuario Invitado" : TodoItems.Usuarios.FirstOrDefault(t => t.id == auditoria.UserId).Email,

                                                Description = auditoria.Description,
                                                RoleId = auditoria.RoleId,
                                                Url = auditoria.Url,
                                                UserId = auditoria.UserId



                                            }).ToList();
            return lista;
        }



        [HttpGet]
        [Route("api/Auditor/listarAuditoria")]
        public IEnumerable<AuditorViewModel> listarAuditoria()
        {

          

            List<AuditorViewModel> lista = (from auditoria in todoItemsAuditor.AuditorLog


                                            select new AuditorViewModel
                                            {
                                                Id = auditoria.Id,
                                                Consult = auditoria.Consult,
                                                CreationDate = auditoria.CreationDate,
                                                email= string.IsNullOrEmpty(TodoItems.Usuarios.FirstOrDefault(t => t.id == auditoria.UserId).Email)?"usuario Invitado": TodoItems.Usuarios.FirstOrDefault(t=>t.id==auditoria.UserId).Email,

                                                Description = auditoria.Description,
                                                RoleId = auditoria.RoleId,
                                                Url = auditoria.Url,
                                                UserId = auditoria.UserId



                                            }).ToList();
            return lista;


        }


        [HttpGet]
        [Route("api/Auditor/GetAuditoryType")]
        public IEnumerable<AuditoryTypeViewModel> GetAuditoryType()
        {


            List<AuditoryTypeViewModel> lista = new List<AuditoryTypeViewModel>();
             List<string> listado = todoItemsAuditor.AuditorLog.Select(t => t.Consult).Distinct().ToList();

            for (int i = 0; i <listado.Count; i++)
            {
                lista.Add(new AuditoryTypeViewModel() { id = i+1, tipo = listado[i] });
            }
            return lista;


        }


        [HttpGet]
        [Route("api/Auditor/EliminarAuditoria/{idAuditoria}")]
        public int EliminarPagina(int idAuditoria)
        {
            int respuesta = 0;

            try
            {

                //Auditor auditor = todoItemsAuditor.GetAuditorById(idAuditoria); ;
                todoItemsAuditor.DeleteAuditor(idAuditoria);

            }
            catch (Exception)
            {

                respuesta = 0;
            }

            return respuesta;
        }

        //public static void SaveAuditoria(string Consult, string URL, string Description)
        //{
        //    AuditorController auditorController = new AuditorController(todoItemsAuditor);
        //    auditorController.AuditorSave(Consult,URL,Description);
        //}

        public   void AuditorSave(string Consult,string URL, string Description)
        {
            try
            {
                int tipoUsuario, idusuario = 0;

                if (HttpContext==null)
                {
                //    if (string.IsNullOrEmpty(HttpContext.Session.GetString("usuario")))
                //{
                    idusuario = 11;
                    tipoUsuario = 5;
                }
                else {
             idusuario = int.Parse(HttpContext.Session.GetString("usuario"));
                    tipoUsuario = int.Parse(HttpContext.Session.GetString("tipoUsuario"));

                }
                //if (string.IsNullOrEmpty(HttpContext.Session.GetString("tipoUsuario")))
                //{
                //    tipoUsuario = 99999;
                //}
                //else {

                //     tipoUsuario = int.Parse(HttpContext.Session.GetString("tipoUsuario"));

                //}

                   
            DateTime CreationDate = DateTime.Now;

                Auditor auditor = new Auditor();
                auditor.Consult = Consult;
                auditor.CreationDate = CreationDate;
                auditor.Description = Description;
                auditor.RoleId = tipoUsuario;
                auditor.UserId = idusuario;
                auditor.Url = URL;
                todoItemsAuditor.AddAuditor(auditor);
               
            }
            catch (Exception)
            {

                throw;
            }
            //,[Consult]
            //,[Url]
            //,[Description]

        }
    }
}