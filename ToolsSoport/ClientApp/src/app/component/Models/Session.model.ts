import { User } from "./user.model";

export class Session {
  public token: string;
  public sessionToken: string;
  
  public message: string;
  public user: User;
}
