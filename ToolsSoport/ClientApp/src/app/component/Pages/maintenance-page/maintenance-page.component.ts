import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-maintenance-page',
  templateUrl: './maintenance-page.component.html',
  styleUrls: ['./maintenance-page.component.css']
})
export class MaintenancePageComponent implements OnInit {
  nombrePagina: string = "Listado de Paginas";
  tituloSuperior: string = "Adicione una nueva pagina";
  constructor() { }

  ngOnInit() {
  }

}
