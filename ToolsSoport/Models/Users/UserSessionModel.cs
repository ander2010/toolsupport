﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ToolsSoport.Models.Usuarios;

namespace ToolsSoport.Models
{
    public class UserSessionModel
    {
        public string userId { get; set; }
        public string sessionToken { get; set; }
        public bool succes { get; set; }
        public string message { get; set; }
        public Users user { get; set; }
    }
}
