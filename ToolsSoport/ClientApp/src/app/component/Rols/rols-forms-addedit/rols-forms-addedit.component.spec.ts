import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RolsFormsAddeditComponent } from './rols-forms-addedit.component';

describe('RolsFormsAddeditComponent', () => {
  let component: RolsFormsAddeditComponent;
  let fixture: ComponentFixture<RolsFormsAddeditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RolsFormsAddeditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RolsFormsAddeditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
