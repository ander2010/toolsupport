import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-maintenance-application',
  templateUrl: './maintenance-application.component.html',
  styleUrls: ['./maintenance-application.component.css']
})
export class MaintenanceApplicationComponent implements OnInit {
  nombrePagina: string = "Listado de Applicativos";
  tituloSuperior: string = "Adicione una Categoria de aplicativo";
  constructor() { }

  ngOnInit() {
  }

}
