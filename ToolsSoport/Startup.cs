using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SpaServices.AngularCli;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using ToolsSoport.Models;
using ToolsSoport.Models.Applicative;
using ToolsSoport.Models.ApplicativeRoleUserPages;
using ToolsSoport.Models.Auditors;
using ToolsSoport.Models.Categories;
using ToolsSoport.Models.Paginas;
using ToolsSoport.Models.Permiso;
using ToolsSoport.Models.PersonaRol;
using ToolsSoport.Models.Rols;
using ToolsSoport.Models.Usuarios;

namespace ToolsSoport
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
            services.AddDistributedMemoryCache();
            services.AddSession(options => {
                options.IdleTimeout = TimeSpan.FromSeconds(3600);
            });

            // services.AddCaching(); // Adds a default in-memory implementation of     IDistributedCache
            //services.AddSession();
            // In production, the Angular files will be served from this directory
            services.AddSpaStaticFiles(configuration =>
            {
                configuration.RootPath = "ClientApp/dist";
            });
            services.AddDbContext<ToolSoportContext>(options => options.UseSqlServer(Configuration["DefaultConnection"]));
            services.AddTransient<IRolRepository, RolRepository>();
            services.AddTransient<IApplicativeUserRolesRepository, ApplicativeUserRolesRepository>();
            services.AddTransient<IUsuarioRepository, UsuarioRepository>();
            services.AddTransient<IPermisoRepository, PermisoRepository>();
            services.AddTransient<IPermisoRolRepository, PermisoRolRepository>();
            services.AddTransient<IAuditorRepository, AuditorRepository>();
            services.AddTransient<IApplicativeRepository, ApplicativeRepository>();
            services.AddTransient<IPageRepository, PageRepository>();
            services.AddTransient<ICategoriesRespository, CategoriesRepository>();
            services.AddTransient< IApplicativeRoleUserPageRepository, ApplicativeRoleUserPageRepository> ();


        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
            }

            app.UseStaticFiles();
            app.UseSpaStaticFiles();
            app.UseSession();
            
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller}/{action=Index}/{id?}");
            });

            app.UseSpa(spa =>
            {
                // To learn more about options for serving an Angular SPA from ASP.NET Core,
                // see https://go.microsoft.com/fwlink/?linkid=864501

                spa.Options.SourcePath = "ClientApp";

                if (env.IsDevelopment())
                {
                    spa.UseAngularCliServer(npmScript: "start");
                }
            });
        }
    }
}
