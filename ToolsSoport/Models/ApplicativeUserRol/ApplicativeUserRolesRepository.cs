﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ToolsSoport.Models;

namespace ToolsSoport.Models.Rols
{
    public class ApplicativeUserRolesRepository : IApplicativeUserRolesRepository
    {

        ToolSoportContext context;
        public ApplicativeUserRolesRepository(ToolSoportContext ctx)
        {
            context = ctx;
        }

        
        public IEnumerable<ApplicativeUserRoles> ApplicativeUserRoles => context.ApplicativeUserRoles;

        public void AddApplicativeUserRol(ApplicativeUserRoles e)
        {
            context.ApplicativeUserRoles.Add(e);
            context.SaveChanges();
        }



        //public void AddRol(UserRol e)
        //{
        //    throw new NotImplementedException();
        //}

        //public void DeleteRol(int RolID)
        //{
        //    throw new NotImplementedException();
        //}

        //public void EditRol(UserRol e)
        //{
        //    throw new NotImplementedException();
        //}

        //public UserRol GetRolById(int RolID)
        //{
        //    throw new NotImplementedException();
        //}
    }
}
