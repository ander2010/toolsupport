﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ToolsSoport.Models.ApiRestViewModel
{
    public class GeneralProcedureViewModel
    {

     public   List<DocumentViewModel> DocumentViewModels { get; set; }
        public List<GrantorSettingViewModel> GrantorSettingViewModels { get; set; }
        public List<PaymentViewModel> PaymentViewModels { get; set; }
        public List<ServiceViewModel> ServiceViewModels { get; set; }

        public  List<PackageViewModel> PackageViewModels { get; set; }

    }
}