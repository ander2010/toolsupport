﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ToolsSoport.Models.Permiso;

namespace ToolsSoport.Models.Usuarios
{
    public class SeguridadViewModel
    {

        public string clave { get; set; }
        public string valor { get; set; }
        public List<PermisoViewModel> listaPagina { get; set; }
    }
}
