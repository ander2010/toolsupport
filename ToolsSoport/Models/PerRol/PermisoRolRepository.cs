﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ToolsSoport.Models.PersonaRol
{
    public class PermisoRolRepository : IPermisoRolRepository
    {

        ToolSoportContext context;
        public PermisoRolRepository(ToolSoportContext ctx)
        {
            context = ctx;
        }

        public IEnumerable<PermisosRol> PermisoRol => context.PermisosRoles;

        public void AddPermisoRol(PermisosRol e)
        {
            context.PermisosRoles.Add(e);
            context.SaveChanges();
        }

        public void DeletePermisoRol(int permisoID)
        {
            PermisosRol e = new PermisosRol() { id = permisoID };
            context.PermisosRoles.Remove(e);
            context.SaveChanges();
        }

        public void EditPermisoRol(PermisosRol e)
        {
            PermisosRol dbEntry = context.PermisosRoles.FirstOrDefault(o => o.id == e.id);
            if (dbEntry != null)
            {
                dbEntry.IdRol = e.IdRol;
                dbEntry.IdPage = e.IdPage;
                //dbEntry.IidpaginaNavigation = e.IidpaginaNavigation;
                //dbEntry.IidRolNavigation = e.IidRolNavigation;
                dbEntry.IsActive = e.IsActive;


            }
            context.SaveChanges();
        }

        public PermisosRol GetPermisoRolById(int permisoID)
        {
            throw new NotImplementedException();
        }
    }
}
