export class Permisos {

  public Id: Int32Array;
  public Message: string;
  public Accion: string;
  public IsActive:boolean;
  public IsVisible: boolean;
}
