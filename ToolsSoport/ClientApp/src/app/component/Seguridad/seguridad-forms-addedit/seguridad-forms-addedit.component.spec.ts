import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SeguridadFormsAddeditComponent } from './seguridad-forms-addedit.component';

describe('SeguridadFormsAddeditComponent', () => {
  let component: SeguridadFormsAddeditComponent;
  let fixture: ComponentFixture<SeguridadFormsAddeditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SeguridadFormsAddeditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SeguridadFormsAddeditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
