﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ToolsSoport.Models.Categories;

namespace ToolsSoport.Models.ApiRestViewModel
{
    public class ApplicativeCategoriesPagesViewModel
    {


        public int UserId { get; set; }
        public string UserName { get; set; }
        public string NombreRol { get; set; }
        public int RolId { get; set; }
        public string AplicativeName { get; set; }

        public int AplicativId { get; set; }

        public string PermissionName { get; set; }
        public int PermissionId { get; set; }

        public string PageName { get; set; }
        public int PageId { get; set; }
        public string Categoriename{get;set;}
        public int Categorieid { get; set; }





    }
}
