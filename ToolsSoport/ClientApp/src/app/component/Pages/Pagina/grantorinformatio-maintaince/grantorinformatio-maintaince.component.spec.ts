import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GrantorinformatioMaintainceComponent } from './grantorinformatio-maintaince.component';

describe('GrantorinformatioMaintainceComponent', () => {
  let component: GrantorinformatioMaintainceComponent;
  let fixture: ComponentFixture<GrantorinformatioMaintainceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GrantorinformatioMaintainceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GrantorinformatioMaintainceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
