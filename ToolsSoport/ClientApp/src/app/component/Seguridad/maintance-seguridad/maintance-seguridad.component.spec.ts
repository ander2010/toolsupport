import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MaintanceSeguridadComponent } from './maintance-seguridad.component';

describe('MaintanceSeguridadComponent', () => {
  let component: MaintanceSeguridadComponent;
  let fixture: ComponentFixture<MaintanceSeguridadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MaintanceSeguridadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MaintanceSeguridadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
