﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ToolsSoport.Models.Usuarios
{
    public class UsuarioRepository : IUsuarioRepository
    {
        ToolSoportContext context;

        public UsuarioRepository(ToolSoportContext ctx)
        {
            context = ctx;
        }
        public IEnumerable<Users> Usuarios => context.Usuarios;

        public void AddUser(Users e)
        {
            context.Usuarios.Add(e);
            context.SaveChanges();
        }

        public void DeleteUser(int UserID)
        {
            Users e = new Users() { id = UserID };
            e.IsActive = false;
            context.Usuarios.Add(e);
            context.SaveChanges(); ;
        }

        public void EditUser(Users e)
        {
            Users dbEntry = context.Usuarios.FirstOrDefault(o => o.id == e.id);
            if (dbEntry != null)
            {
                //dbEntry.idRolNavigation = e.idRolNavigation;
                dbEntry.Email = e.Email;
                dbEntry.Name = e.Name;
                dbEntry.IsActive = e.IsActive;
                dbEntry.UserName = e.UserName;
                dbEntry.Password = e.Password;
                dbEntry.CreationDate = DateTime.Now;
                dbEntry.ModificationDate = DateTime.Now;



            }
            context.SaveChanges();
        }

        public Users GetUserById(int UserID)
        {
            Users e = context.Usuarios.FirstOrDefault(t => t.id == UserID);
            return e;
        }

        public bool login(string user, string password)
        {
            int respuesta = 0;
            UsuarioViewModel usuarioView = new UsuarioViewModel();

            respuesta = context.Usuarios.Where(t => t.Email.ToUpper() == user.ToUpper() && t.Password == password).Count();

            if (respuesta == 1)
            {
                //Users ouser = TodoItems.Usuarios.Where(t => t.Nombreusuario.ToUpper() == usuario.nombreUsuario.ToUpper() && t.Contra == cifradaPassword).FirstOrDefault();
                //HttpContext.Session.SetString("usuario", ouser.id.ToString());
                //HttpContext.Session.SetString("tipoUsuario", ouser.Iidtipousuario.ToString());
                //usuarioView.id = ouser.id;
                //usuarioView.nombreUsuario = usuario.nombreUsuario;
                return true;
            }
            else
            {
                return false;
            }

           

          
        }
    }
}
