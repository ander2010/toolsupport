﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ToolsSoport.Models.Paginas
{
    public class PageViewModel
    {
                
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }        
        public int CategoriesId { get; set; }
        public int PermisoId { get; set; }
        public bool isActive { get; set; }
        public string namePermiso { get; set; }

        
    }
}
