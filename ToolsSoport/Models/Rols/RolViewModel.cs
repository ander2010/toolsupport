﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ToolsSoport.Models.Paginas;
using ToolsSoport.Models.Permiso;

namespace ToolsSoport.Models.Rols
{
    public class RolViewModel
    {

        public int id { get; set; }

       
        public string name { get; set; }

        public string description { get; set; }

        public Boolean isActive { get; set; }
        public string valores { get; set; }
        //public List<PermisoViewModel> listapagina { get; set; }
    }
}
