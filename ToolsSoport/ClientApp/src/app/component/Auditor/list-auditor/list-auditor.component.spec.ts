import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListAuditorComponent } from './list-auditor.component';

describe('ListAuditorComponent', () => {
  let component: ListAuditorComponent;
  let fixture: ComponentFixture<ListAuditorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListAuditorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListAuditorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
