﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ToolsSoport.Models.Categories
{
    public interface ICategoriesRespository
    {



        IEnumerable<Categorie> Categories { get; }

        void AddCategories(Categorie e);

        void EditCategories(Categorie e);

        void DeleteCategories(int CateogoriesID);
        Categorie GetCategoriesById(int CategoriesID);


    }
}
