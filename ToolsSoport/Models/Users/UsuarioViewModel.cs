﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ToolsSoport.Models.Usuarios
{
    public class UsuarioViewModel
    {

        public int id { get; set; }
        public string nombreUsuario { get; set; }
        public string email { get; set; }
        public string nombrepersona { get; set; }
        public bool habilitado { get; set; }

        public string nombretipoUsuario { get; set; }
        public int idPersona { get; set; }
        public int idtipousuario { get; set; }
        public string contra { get; set; }
        public string contra2 { get; set; }

        public int idRol { get; set; }
        public int idApplicative { get; set; }

    }
}
