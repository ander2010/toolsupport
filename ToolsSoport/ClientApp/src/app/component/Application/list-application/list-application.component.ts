import { Component, OnInit, Input } from '@angular/core';
import { ApplicationService } from '../../../services/application.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'list-application',
  templateUrl: './list-application.component.html',
  styleUrls: ['./list-application.component.css']
})
export class ListApplicationComponent implements OnInit {

  @Input() applicatives: any;
  @Input() isMantenimiento = false;
  cabeceras: string[] = ["Id pagina", "Mensaje", "Accion"]
  constructor(private applicationservice: ApplicationService, private ruta: Router, private activateroute: ActivatedRoute) { }

  ngOnInit() {
    this.applicationservice.listarApplicative().subscribe(t => this.applicatives = t);
    console.info(this.applicatives)
  }


  eliminarAplication(id) {

    this.applicationservice.EliminarApplicative(id).subscribe(t => {

      this.applicationservice.listarApplicative().subscribe(t => this.applicatives = t);
    });
  }
}
