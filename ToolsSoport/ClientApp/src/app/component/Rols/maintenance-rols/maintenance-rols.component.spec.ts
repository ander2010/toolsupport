import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MaintenanceRolsComponent } from './maintenance-rols.component';

describe('MaintenanceRolsComponent', () => {
  let component: MaintenanceRolsComponent;
  let fixture: ComponentFixture<MaintenanceRolsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MaintenanceRolsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MaintenanceRolsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
