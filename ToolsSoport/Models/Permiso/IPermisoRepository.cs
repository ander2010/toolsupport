﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ToolsSoport.Models.Permiso
{
    public interface IPermisoRepository
    {
        IEnumerable<Permisos> Permission { get; }

        void AddPermission(Permisos e);

        void EditPermission(Permisos e);

        void DeletePermission(int permisoID);
        Permisos GetPermissionById(int permisoID);

    }
}
