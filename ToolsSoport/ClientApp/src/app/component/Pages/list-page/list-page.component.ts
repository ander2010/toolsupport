import { Component, OnInit, Input } from '@angular/core';
import { UsuarioService } from '../../../services/usuario.service';
import { ApplicationService } from '../../../services/application.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'list-page',
  templateUrl: './list-page.component.html',
  styleUrls: ['./list-page.component.css']
})
export class ListPageComponent implements OnInit {
  @Input() paginas: any;
  @Input() isMantenimiento = false;
  cabeceras: string[] = ["Id pagina", "Mensaje", "Accion"]
  constructor(private usuarioservice: UsuarioService, private ruta: Router, private activateroute: ActivatedRoute, private applicationservice: ApplicationService) { }

  ngOnInit() {
    this.applicationservice.ApplicativePageList().subscribe(t => this.paginas = t);

  }


  eliminarpagina(id) {

    this.applicationservice.EliminarApplicativePage(id).subscribe(t => {
      this.applicationservice.ApplicativePageList().subscribe(t => this.paginas = t);
      //this.usuarioservice.listarpaginas().subscribe(t => this.paginas = t);
    });

  }

}
