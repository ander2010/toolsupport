import { Injectable, Inject } from "@angular/core";
import { Http, Response, RequestOptions } from "@angular/http";
import { Observable } from "rxjs";
//import { LoginObject } from "./login-object.model";
//import { Session } from "../../core/models/session.model";
//import { StorageService } from '../../../servicios/storage.service';
import { Headers } from '@angular/http';
import { t } from "@angular/core/src/render3";
import { PublicLogin } from "../component/Models/PublicLogin.model";
//import { PublicLogin } from "../../core/models/PublicLogin.model";

@Injectable()
export class AuthenticationService {
  //private storageservice: StorageService

  urlBase: string;
  constructor(private http: Http, @Inject('BASE_URL') baseUrl: string ) {

    this.urlBase = baseUrl;

  }
  private basePath = 'api/Autenticacion/';
  //login(loginObj: LoginObject): Observable<Session> {
  //  return this.http.post(this.urlBase + 'api/Autenticacion/OauthLogin', loginObj).map(this.extractData);
  //}
  logout(): Observable<Boolean> {
    return this.http.post(this.basePath + 'logout', {}).map(this.extractData);
  }

  public refreshToken(refresh_token) {


    // http://localhost:5000/api/Autenticacion/RefreshToken ? refresh_token = 22222
    const postUrlee = this.urlBase + 'api/Autenticacion/RefreshToken?refresh_token=' + refresh_token;


    //let body = JSON.stringify({
    //  "user": "pociam",

    //  "password": "BizperantO1"

    //});

    let headers = new Headers({ 'Content-Type': 'application/json' });
    console.log(refresh_token);
    let options = new RequestOptions({ headers: headers });
    return this.http.post(postUrlee, options)
      .map(t => this.extractData3(t))





    ///  this.http.post();

  }


  private extractData3(res: Response) {
    let body = res.json();
    console.info(body);
    return body || {};
  }

  public loginPublic(username, password) {

    const postUrlee = this.urlBase + 'api/Users/PublicLogin';
    var publiclogin = new PublicLogin();
    publiclogin.user = username;
    publiclogin.password = password;
    let body = JSON.stringify({
      "user": username,

      "password": password

    });

    let headers = new Headers({ 'Content-Type': 'application/json' });
    console.log(body);
    let options = new RequestOptions({ headers: headers });
    return this.http.post(postUrlee, body, options)
      .map(t => this.extractData2(t))

  }



  //console.info(JSON.stringify(publiclogin));
  //return this.http.post(postUrl, JSON.stringify(publiclogin));

  private extractData2(res: Response) {
    let body = res.json();
    console.info(body);
    return body || {};
  }


  private handleErrorObservable(error: Response | any) {
    console.error(error.message || error);
    return Observable.throw(error.message || error);
  }



  public login(loginObj) {
    console.info(this.urlBase);
    return this.http.post(this.urlBase + 'api/Users/OauthLogin', loginObj).map(


      t => this.extractData(t));

  }
  private extractData(res: Response) {
    let body = res.json();
    return body;
  }

}
