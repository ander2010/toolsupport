import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { AuditorService } from '../../../services/auditor.service'


@Component({
  selector: 'app-maintenance-auditor',
  templateUrl: './maintenance-auditor.component.html',
  styleUrls: ['./maintenance-auditor.component.css']
})
export class MaintenanceAuditorComponent implements OnInit {
  nombrePagina: string = "Listado de Auditoria";
  tituloSuperior: string = "Listado de Auditoria";
  UsertypeCombo: any;
  @Output() FiltertypeAuditoria: EventEmitter<any>;
  auditoria: any;

  constructor(private auditorService: AuditorService) {
    this.FiltertypeAuditoria = new EventEmitter();
}

  ngOnInit() {

    this.auditorService.GetAuditoryType().subscribe(t => this.UsertypeCombo = t);

  }


  filtrar(tipofilter) {

    if (tipofilter == "") {
      this.auditorService.listarAuditoria().subscribe(t => this.auditoria = t);

    }
    else {

      this.auditorService.AuditoryFilter(tipofilter.value).subscribe(t => this.auditoria = t);
    }


  }


}
