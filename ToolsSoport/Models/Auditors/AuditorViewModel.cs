﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ToolsSoport.Models.Auditors
{
    public class AuditorViewModel
    {

        public int Id { get; set; }
        public int RoleId { get; set; }
        public int UserId { get; set; }

        public string email { get; set; }
        public DateTime CreationDate { get; set; }
        public string Consult { get; set; }


        public string Url { get; set; }

        public string Description { get; set; }

    }
}
