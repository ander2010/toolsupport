import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { AuthenticationService } from '../../services/authentication.service';
import { Router } from '@angular/router';
import { UserSession } from '../Models/UserSession.model';
import { LoginObject } from '../Models/LoginObject.model';
import { Session } from '../Models/Session.model';
import { StorageService } from '../../services/storage.service';
import { UsuarioService } from '../../services/usuario.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  public loginForm: FormGroup;
  public submitted: Boolean = false;
  title: string = "Login";
  Errorlog: string = "";
  urlbasemia: any;
  ListadoPagina: any;
  public error: { code: number, message: string } = null;
  constructor(private formBuilder: FormBuilder,
    private authenticationService: AuthenticationService,
    //private storageService: StorageService,
    private router: Router, @Inject("BASE_URL") urlbase: string, private usuarioservice: UsuarioService, private storageService: StorageService) {
    this.urlbasemia = urlbase;
    this.loginForm = new FormGroup({
      "username": new FormControl("", Validators.required),
      "password": new FormControl("", Validators.required),
      //"client_secret": new FormControl("", Validators.required),
      //"client_id": new FormControl("", Validators.required),
      //"grant_type": new FormControl("password", Validators.required),

    });



  }
  ngOnInit() {
    //this.loginForm = this.formBuilder.group({
    //  username: ['', Validators.required],
    //  password: ['', Validators.required],
    //})
  }
  public submitLogin(): void {




    this.submitted = true;
    this.error = null;
    if (this.loginForm.valid) {

      var InfoResto = new UserSession();
      var username = this.loginForm.controls["username"].value;
      var password = this.loginForm.controls["password"].value;
      this.authenticationService.loginPublic(username, password).subscribe(
        data => this.correctLogin(data, InfoResto),
        error => this.error = JSON.parse(error._body)



       
      )


      //this.authenticationService.login(new LoginObject(this.loginForm.value)).subscribe(
      //  data => this.correctLogin(data, InfoResto),    
      //  error => this.error = JSON.parse(error._body)
      //)
    }
  }



  LoginNormal(InfoResto: UserSession) {

    console.info(InfoResto);
    this.authenticationService.login(new LoginObject(this.loginForm.value)).subscribe(
      data => this.correctLogin(data, InfoResto = data),

      error => this.error = JSON.parse(error._body)
    )
   // this.usuarioservice.ListarPagina().subscribe(t => this.ListadoPagina = t);
   // console.info(this.ListadoPagina);
  }

  private correctLogin(data: Session, InfoResto: UserSession) {



    console.info(data);
    console.info(InfoResto);
    var sesion = new Session();

    if (data.user != null) {


      sesion.message = data.message;

      sesion.token = data.sessionToken;
      sesion.user = data.user;

     


      this.storageService.setCurrentSession(sesion);


      window.location.href = "/home";
      this.router.navigate(['/home']);
    } else {

      this.Errorlog =   data.message ;
    }
  }

}
