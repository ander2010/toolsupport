import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LogMaintanceComponent } from './log-maintance.component';

describe('LogMaintanceComponent', () => {
  let component: LogMaintanceComponent;
  let fixture: ComponentFixture<LogMaintanceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LogMaintanceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LogMaintanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
