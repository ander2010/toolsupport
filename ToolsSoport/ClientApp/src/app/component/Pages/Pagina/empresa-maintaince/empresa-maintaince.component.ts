import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { ApplicationService } from '../../../../services/application.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-empresa-maintaince',
  templateUrl: './empresa-maintaince.component.html',
  styleUrls: ['./empresa-maintaince.component.css']
})
export class EmpresaMaintainceComponent implements OnInit {
  ver: boolean = false;
  parametro: string;
  ProcedureViewModel: any;
  grantorIdentificator: string;
  borrado: any;
  nombrePagina: string = "Detalles de la empresa";
  tituloSuperior: string = "Detalles y buscador de Empresa";
  searchGrantor: FormGroup;
  constructor(private applicationservice: ApplicationService, private ruta: Router, private activateroute: ActivatedRoute) {
    this.searchGrantor = new FormGroup({

      "grantorId": new FormControl("A38D5272-4053-44B2-BAE7-40EF748820F2", Validators.required)

    });

  }
  ngOnInit() {


  }


  Cambiarbuscar() {
    this.grantorIdentificator = this.searchGrantor.controls["grantorId"].value;
    this.applicationservice.buscarEmpresaGrantor(this.searchGrantor.controls["grantorId"].value).subscribe(t => this.ProcedureViewModel = t);
    console.info(this.ProcedureViewModel);

    this.ver = true;
  }


  eliminarEmpresa(identificador) {
    if (confirm('Realmente desea eliminar la empresa')) {
      console.info(identificador);
      this.applicationservice.DeleteCompany(identificador).subscribe(t => this.borrado = t);
      this.ver = false;
    }
   
  }

}
