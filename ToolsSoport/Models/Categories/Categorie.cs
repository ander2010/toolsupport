﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ToolsSoport.Models.Categories
{
    [Table("Categories")]
    public class Categorie
    {

        public int Id { get; set; }
        public string Name { get; set; }

        public int ApplicationId { get; set; }
    }
}
