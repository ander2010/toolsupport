﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ToolsSoport.Models.Paginas;
using ToolsSoport.Models.Permiso;
using ToolsSoport.Models.PersonaRol;
using ToolsSoport.Models.Rols;

namespace ToolsSoport.Controllers
{
    //[Route("api/[controller]")]
    //[ApiController]
    public class RolController : ControllerBase
    {



        IRolRepository todoitems { get; set; }
        IPermisoRepository todoitemsPermiso { get; set; }
        IPermisoRolRepository todoItemPermisoRol { get; set; }

        public RolController(IRolRepository todoRoles, IPermisoRepository todoitemsPaginas, IPermisoRolRepository todoItemPaginaRoles)
        {
            todoitems = todoRoles;
            todoitemsPermiso = todoitemsPaginas;
            todoItemPermisoRol = todoItemPaginaRoles;
        }
        //public IActionResult Index()
        //{
        //    return View();
        //}


        [HttpGet]
        [Route("api/Rol/ListarTipoUsuario")]
        public List<RolViewModel> ListarTipoUsuario()
        {

            List<RolViewModel> listatipousuario = new List<RolViewModel>();

            listatipousuario = (todoitems.Roles.Where(t => t.IsActive == true).Select(t => new RolViewModel
            {
                id = t.Id,
                name = t.Name,
                description = t.Description,
                isActive = t.IsActive,
               

            })).ToList();

            return listatipousuario;

        }



        [HttpGet]
        [Route("api/Rol/listarPaginastipoUsuario")]
        public List<PermisoViewModel> listarPaginastipoUsuario()
        {
            List<PermisoViewModel> listpaginaViews = new List<PermisoViewModel>();


            listpaginaViews = (from paginas in todoitemsPermiso.Permission
                               where
     paginas.IsActive == true
                               select new PermisoViewModel
                               {
                                   Id = paginas.Id,
                                   Message = paginas.Message
                               }).ToList();

            return listpaginaViews;

        }
        [HttpGet]
        [Route("api/Rol/ListarpaginaRecuperadas/{idtipousuario}")]
        public RolViewModel ListarpaginaRecuperadas(int idtipousuario)
        {
            RolViewModel RolViewModela = new RolViewModel();

            List<PermisoViewModel> listapaginas = (from tipous in todoitems.Roles
                                                  join pagina in todoItemPermisoRol.PermisoRol
                                                  on tipous.Id equals pagina.IdRol
                                                  join Pagin in todoitemsPermiso.Permission on pagina.IdPage equals Pagin.Id
                                                  where pagina.IdRol == idtipousuario
                                                  && pagina.IsActive == true
                                                  select new PermisoViewModel
                                                  {
                                                      Id = Pagin.Id
                                                  }
                                                ).ToList();
            UserRol tipoUsuario = todoitems.Roles.Where(t => t.Id == idtipousuario).FirstOrDefault();
            RolViewModela.id = tipoUsuario.Id;
            RolViewModela.name = tipoUsuario.Name;

            RolViewModela.description = tipoUsuario.Description;
            //RolViewModela.listapagina = listapaginas;

            return RolViewModela;


        }



        [HttpPost]
        [Route("api/Rol/guardarDatosTipoUsuario")]
        public int guardarDatosTipoUsuario([FromBody]RolViewModel tipoUsuarioViewModel)
        {

            int rpta = 0;
            try
            {

                if (tipoUsuarioViewModel.id == 0)
                {
                    UserRol tipoUsuario = new UserRol();
                    tipoUsuario.Name = tipoUsuarioViewModel.name;
                    tipoUsuario.Description = tipoUsuarioViewModel.description;
                    tipoUsuario.IsActive = true;
                    todoitems.AddRol(tipoUsuario);

                    int idTipoUsuario = tipoUsuario.Id;
                    string[] ids = tipoUsuarioViewModel.valores.Split("$");
                    for (int i = 0; i < ids.Length; i++)
                    {
                        PermisosRol paginaTipo = new PermisosRol();
                        paginaTipo.IdPage = int.Parse(ids[i]);
                        paginaTipo.IdRol = idTipoUsuario;
                        paginaTipo.IsActive = true;
                        todoItemPermisoRol.AddPermisoRol(paginaTipo);
                    }

                    rpta = 1;

                }
                else
                {

                    UserRol tipoUsuario = todoitems.Roles.FirstOrDefault(t => t.Id == tipoUsuarioViewModel.id);
                    tipoUsuario.Name = tipoUsuarioViewModel.name;
                    tipoUsuario.Description = tipoUsuarioViewModel.description;
                    todoitems.EditRol(tipoUsuario);
                    string[] ids = tipoUsuarioViewModel.valores.Split("$");
                    List<PermisosRol> lista = todoItemPermisoRol.PermisoRol.Where(t => t.IdRol == tipoUsuarioViewModel.id).ToList();
                    foreach (var pag in lista)
                    {
                        pag.IsActive = false;
                    }
                    int cantidad;
                    for (int i = 0; i < ids.Length; i++)
                    {
                        cantidad = lista.Where(t => t.IdPage == int.Parse(ids[i])).Count();
                        if (cantidad == 0)
                        {

                            PermisosRol  paginaTipo = new PermisosRol();
                            paginaTipo.IdPage = int.Parse(ids[i]);
                            paginaTipo.IdRol = tipoUsuarioViewModel.id;
                            paginaTipo.IsActive = true;
                            todoItemPermisoRol.AddPermisoRol(paginaTipo);
                            //todoItemPaginaRol.AddPagina(paginaTipo);

                        }
                        else
                        {
                            PermisosRol op = lista.Where(t => t.IdPage == int.Parse(ids[i])).FirstOrDefault();
                            op.IsActive =true;
                            //todoItemPaginaRol.EditPagina(op);
                            todoItemPermisoRol.EditPermisoRol(op);

                        }
                    }

                    //bd.SaveChanges();
                    //transaccion.Complete();
                    rpta = 1;
                }


            }
            catch (Exception)
            {

                rpta = 0
                ;
            }
            return rpta;
        }


        [HttpGet]
        [Route("api/Rol/eliminarTipoUsuario/{idTipoUsuario}")]
        public int eliminarTipoUsuario(int idTipoUsuario)
        {
            int rpta = 0;
            try
            {

                UserRol tipoUsuario = todoitems.Roles.FirstOrDefault(t => t.Id == idTipoUsuario);
                tipoUsuario.IsActive = false;
                todoitems.EditRol(tipoUsuario);
                rpta = 1;

            }
            catch (Exception)
            {

                rpta = 0;
            }
            return rpta;

        }

    }
}