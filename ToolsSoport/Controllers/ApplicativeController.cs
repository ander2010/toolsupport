﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ToolsSoport.Models.Applicative;
using ToolsSoport.Models.Paginas;
using ToolsSoport.Models.Auditors;
using System.Net;
using System.IO;
using Newtonsoft.Json;
using ToolsSoport.Models.ApiRestViewModel;
using ToolsSoport.Helpers;
using ToolsSoport.Models.ApplicativeRoleUserPages;
using ToolsSoport.Models.Permiso;
using ToolsSoport.Models.Rols;
using ToolsSoport.Models.Usuarios;

namespace ToolsSoport.Controllers
{



    public class ApplicativeController : ControllerBase
    {

        IApplicativeRepository todoItemsApplicative { get; set; }
        IRolRepository todoRol { get; set; }

        IAuditorRepository todoItemsAuditor { get; set; }

        IUsuarioRepository todoUsers { get; set; }
        IPageRepository todoItemsApplicativeSon { get; set; }

        IApplicativeRoleUserPageRepository todoItemsApplicativeRolUserPage { get; set; }


        IPermisoRepository todoItemPermiso { get; set; }

        public ApplicativeController(IApplicativeRepository todoItemsApplicatives, IUsuarioRepository todoUsers , IRolRepository todoRol, IApplicativeRoleUserPageRepository todoItemsApplicativeRolUserPage, IPermisoRepository todoItemPermiso, IAuditorRepository todoItemsAuditores, IPageRepository todoItemsApplicativeSones)
        {
            todoItemsApplicative = todoItemsApplicatives;
            todoItemsApplicativeSon = todoItemsApplicativeSones;
            todoItemsAuditor= todoItemsAuditores;
            this.todoRol = todoRol;
            this.todoItemsApplicativeRolUserPage = todoItemsApplicativeRolUserPage;
            this.todoItemPermiso = todoItemPermiso;
            this.todoUsers = todoUsers;

        }
        [HttpGet]
        [Route("api/Applicative/DownloadString/{idapplicative}")]
        public GeneralProcedureViewModel DownloadString(string idapplicative)
        {
            //string address = "http://localhost:51043/Support/Support/spSupportGeneralGrantorInfo?grantorId=" + idapplicative; //A38D5272-4053-44B2-BAE7-40EF748820F2"
            //WebClient client = new WebClient();
            //string reply = client.DownloadString(address);

            GeneralProcedureViewModel generalProcedureViewModel = new GeneralProcedureViewModel();
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(@"http://localhost:51043/Support/Support/spSupportGeneralGrantorInfo?grantorId=" + idapplicative);
            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            using (Stream stream = response.GetResponseStream())
            using (StreamReader reader = new StreamReader(stream))
            {
                var json = reader.ReadToEnd();
                generalProcedureViewModel = JsonConvert.DeserializeObject<GeneralProcedureViewModel>(json);
            }

            AuditorSave(OperationsController.ApplicativeController_EmpresaConsulta, "http//publiclogin", OperationsDescription.ApplicativeController_EmpresaCosulta+ idapplicative);

            return generalProcedureViewModel;

        }

        [HttpGet]
        [Route("api/Applicative/DeleteCompany/{idapplicative}")]
        public  int DeleteCompany(string idapplicative)
        {
            //string address = "http://localhost:51043/Support/Support/spSupportGeneralGrantorInfo?grantorId=A38D5272-4053-44B2-BAE7-40EF748820F2";
            //WebClient client = new WebClient();
            //string reply = client.DownloadString(address);
            AuditorSave(OperationsController.ApplicativeController_EmpresaBaja, "http//publiclogin", OperationsDescription.ApplicativeController_EmpresaBaja + idapplicative);

            return 1;
        }
        [HttpGet]
        [Route("api/Applicative/saveUserRolApplicative/{idapplicative}/{valor}")]
        public int saveUserRolApplicative(string idapplicative,string valor)
        {
            //string address = "http://localhost:51043/Support/Support/spSupportGeneralGrantorInfo?grantorId=A38D5272-4053-44B2-BAE7-40EF748820F2";
            //WebClient client = new WebClient();
            //string reply = client.DownloadString(address);
            //AuditorSave(OperationsController.ApplicativeController_EmpresaBaja, "http//publiclogin", OperationsDescription.ApplicativeController_EmpresaBaja + idapplicative);

            return 1;
        }

        
        [HttpGet]
        [Route("api/Applicative/GetPagesApplicative")]
        public IEnumerable<DictionaryPageviewModel> GetPagesApplicative()
        {


            List<PageViewModel> lista = (from page in todoItemsApplicativeSon.Pages


                                         select new PageViewModel
                                         {
                                             Id = page.Id,
                                             Name = page.Name,
                                             Description = page.Description,
                                             CategoriesId = page.CategoriesId,
                                             PermisoId = page.PermisoId,
                                             isActive = page.IsActive


                                         }).ToList();

            List<DictionaryPageviewModel> dictionaryPageviewModels = new List<DictionaryPageviewModel>();

            var queryLastNames = from listado in lista
                                 group listado by listado.CategoriesId into newGroup

                                 select newGroup;
            List<PageViewModel> pageViewModelslist;
            foreach (var query in queryLastNames)
            {
                pageViewModelslist = new List<PageViewModel>();
                DictionaryPageviewModel dictionary = new DictionaryPageviewModel();
                dictionary.Name = query.Key.ToString();
                foreach (var querys in query)
                {

                    pageViewModelslist.Add(querys);


                }
                dictionary.PageListAsociateteName = pageViewModelslist;
                dictionaryPageviewModels.Add(dictionary);
                //dictionaryPageviewModels.Add(new DictionaryPageviewModel() { Name = query.Key, PageListAsociateteName = new List<PageViewModel>() { query[query.Key] } })
                // Console.WriteLine($"\t{student.LastName}, {student.FirstName}");
            }


            //for (int i = 0; i < lista.Count; i++)
            //{

            //    if (dictionaryPageviewModels.Count==0)
            //    {
            //        dictionaryPageviewModels.Add(new DictionaryPageviewModel() { Name = lista[i].parentId, PageListAsociateteName=new List<PageViewModel>() { lista[i] } });
            //        continue;
            //    }
            //    for (int j = 0; j < dictionaryPageviewModels.Count; j++)
            //    {
            //        if (lista[i].parentId == dictionaryPageviewModels[j].Name)
            //        {
            //            dictionaryPageviewModels[j].PageListAsociateteName.Add(lista[i]);
            //        }
            //        else
            //        {

            //            dictionaryPageviewModels.Add(new DictionaryPageviewModel() { Name = lista[i].parentId, PageListAsociateteName = new List<PageViewModel>() { lista[i] } } );

            //        }
            //    }


            //}
          //  auditorController.AuditorSave(OperationsController.ApplicativeController_GetPagesApplicative, "http//publiclogin", OperationsDescription.ApplicativeController_GetPagesApplicative);

            return dictionaryPageviewModels;





        }


        [HttpGet]
        [Route("api/Applicative/listarApplicative")]
        public IEnumerable<ApplicativeViewModel> listarApplicative()
        {


            List<ApplicativeViewModel> lista = (from Applicatives in todoItemsApplicative.Applicatives


                                                select new ApplicativeViewModel
                                                {
                                                    Id = Applicatives.Id,
                                                    Name = Applicatives.Name,
                                                    Description = Applicatives.Description
                                                   // ListCategories = todoItemsApplicativeSon.Pages.Where(t => t.ApplicationId == Applicatives.Id).ToList(),


                                                }).ToList();

            //AuditorSave(OperationsController.ApplicativeController_listarApplicative, "http//publiclogin", OperationsDescription.ApplicativeController_listarApplicative);

            return lista;


        }




        [HttpPost]
        [Route("api/Applicative/GuardarDatosson")]
        public int GuardarDatosson([FromBody]PageViewModel ApplicatiViewModel)
        {

            int rpta = 0;
            try
            {

                using (var transaccion = new TransactionScope())
                {
                    if (ApplicatiViewModel.Id == 0)
                    {
                        Page ApplicativesonViewModel = new Page();
                        ApplicativesonViewModel.Name = ApplicatiViewModel.Name;
                        ApplicativesonViewModel.Description = ApplicatiViewModel.Description;
                        ApplicativesonViewModel.IsActive = ApplicatiViewModel.isActive;
                        ApplicativesonViewModel.PermisoId = ApplicatiViewModel.PermisoId;
                        ApplicativesonViewModel.CreationDate = DateTime.Now;
                        ApplicativesonViewModel.ModificationDate = DateTime.Now;
                        //ApplicativesonViewModel.ParentId = todoItemsApplicative.Applicatives.Where(t => t.Id == ApplicatiViewModel.ApplicationId).FirstOrDefault().Name;
                        ApplicativesonViewModel.IsActive = true;

                        todoItemsApplicativeSon.AddPages(ApplicativesonViewModel);
                        transaccion.Complete();
                        rpta = 1;

                       // AuditorSave(OperationsController.ApplicativeController_GuardarDatosson, "http//publiclogin", OperationsDescription.ApplicativeController_GuardarDatosson);

                    }
                    else
                    {
                        Page ApplicativesViewModel = todoItemsApplicativeSon.GetPagesById(ApplicatiViewModel.Id);

                        ApplicativesViewModel.Name = ApplicativesViewModel.Name;

                        ApplicativesViewModel.Description = ApplicativesViewModel.Description;
                        ApplicativesViewModel.CategoriesId = ApplicatiViewModel.CategoriesId;
                        ApplicativesViewModel.PermisoId = ApplicatiViewModel.PermisoId;
                       
                        ApplicativesViewModel.IsActive = true;

                        todoItemsApplicativeSon.EditPages(ApplicativesViewModel);
                        transaccion.Complete();
                        rpta = 1;
                      //AuditorSave(OperationsController.ApplicativeController_GuardarDatosson, "http//publiclogin", "Editar la pagina perteneiente a los aplicativos");

                    }

                }
            }
            catch (Exception)
            {

                rpta = 0
                ;
            }
            return rpta;
        }





        [HttpPost]
        [Route("api/Applicative/GuardarDatos")]
        public int GuardarDatos([FromBody]ApplicativeViewModel ApplicatiViewModel)
        {

            int rpta = 0;
            try
            {

                using (var transaccion = new TransactionScope())
                {
                    if (ApplicatiViewModel.Id == 0)
                    {
                        Applicatives ApplicativesViewModel = new Applicatives();
                        ApplicativesViewModel.Name = ApplicatiViewModel.Name;
                        ApplicativesViewModel.Description = ApplicatiViewModel.Description;
                        ApplicativesViewModel.CreationDate = DateTime.Now;

                        todoItemsApplicative.AddApplicatives(ApplicativesViewModel);
                        transaccion.Complete();
                        rpta = 1;
                       // AuditorSave(OperationsController.ApplicativeController_GuardarDatosson, "http//publiclogin", OperationsDescription.ApplicativeController_GuardarDatosson);
                     

                    }
                    else
                    {
                        Applicatives ApplicativesViewModel = todoItemsApplicative.GetApplicativesById(ApplicatiViewModel.Id);

                        ApplicativesViewModel.Name = ApplicativesViewModel.Name;

                        ApplicativesViewModel.Description = ApplicativesViewModel.Description;
                        ApplicativesViewModel.CreationDate = ApplicativesViewModel.CreationDate;

                        todoItemsApplicative.EditApplicatives(ApplicativesViewModel);
                        transaccion.Complete();
                        rpta = 1;
                       // AuditorSave(OperationsController.ApplicativeController_GuardarDatos, "http//publiclogin", "Editar datos e infomracion sobre el aplicativo");

                    }

                }
            }
            catch (Exception)
            {

                rpta = 0
                ;
            }
            return rpta;
        }







        [HttpGet]
        [Route("api/Applicative/GetApplicativePage/{idapplicative}")]
        public PageViewModel GetApplicativePage(int idapplicative)
        {
            int respuesta = 0;
            PageViewModel pageView = new PageViewModel();

            try
            {

                //Auditor auditor = todoItemsAuditor.GetAuditorById(idAuditoria); ;
                //Page page= todoItemsApplicativeSon.GetPagesById(idapplicative);
                // page.IsActive = false;
                // todoItemsApplicativeSon.EditPages(page);
                Page page = todoItemsApplicativeSon.GetPagesById(idapplicative);
                pageView.Name = page.Name;
                pageView.Id = page.Id;
                pageView.Description = page.Description;
                pageView.CategoriesId = page.CategoriesId;
                pageView.isActive = page.IsActive;

                respuesta = 1;
                //AuditorSave(OperationsController.ApplicativeController_GetApplicativePage, "http//publiclogin", OperationsDescription.ApplicativeController_GetApplicativePage);

            }
            catch (Exception)
            {

                respuesta = 0;
            }

            return pageView;
        }

        [HttpGet]
        [Route("api/Applicative/GetApplicative/{idapplicative}")]
        public PageViewModel GetApplicative(int idapplicative)
        {
            int respuesta = 0;
            PageViewModel pageView = new PageViewModel();

            try
            {

                //Auditor auditor = todoItemsAuditor.GetAuditorById(idAuditoria); ;
                //Page page= todoItemsApplicativeSon.GetPagesById(idapplicative);
                // page.IsActive = false;
                // todoItemsApplicativeSon.EditPages(page);
                Applicatives page = todoItemsApplicative.GetApplicativesById(idapplicative);
                pageView.Name = page.Name;
                pageView.Id = page.Id;
                pageView.Description = page.Description;
                //pageView.parentId = page.ParentId;
                //pageView.isActive = page.IsActive;

                respuesta = 1;
               //AuditorSave(OperationsController.ApplicativeController_GetApplicative, "http//publiclogin", OperationsDescription.ApplicativeController_GetApplicative);

            }
            catch (Exception)
            {

                respuesta = 0;
            }

            return pageView;
        }




        [HttpGet]
        [Route("api/Applicative/DeletePageApplicative/{idapplicative}")]
        public int DeletePageApplicative(int idapplicative)
        {
            int respuesta = 0;

            try
            {

                //Auditor auditor = todoItemsAuditor.GetAuditorById(idAuditoria); ;
                //Page page= todoItemsApplicativeSon.GetPagesById(idapplicative);
                // page.IsActive = false;
                // todoItemsApplicativeSon.EditPages(page);
                todoItemsApplicativeSon.DeletePages(idapplicative);
                respuesta = 1;
               // AuditorSave(OperationsController.ApplicativeController_DeletePageApplicative, "http//publiclogin", OperationsDescription.ApplicativeController_DeletePageApplicative);


            }
            catch (Exception)
            {

                respuesta = 0;
            }

            return respuesta;
        }

        [HttpGet]
        [Route("api/Applicative/DeleteApplicative/{idapplicative}")]
        public int DeleteApplicative(int idapplicative)
        {
            int respuesta = 0;

            try
            {

                //Auditor auditor = todoItemsAuditor.GetAuditorById(idAuditoria); ;
                //Page page= todoItemsApplicativeSon.GetPagesById(idapplicative);
                // page.IsActive = false;
                // todoItemsApplicativeSon.EditPages(page);
                todoItemsApplicative.DeleteApplicatives(idapplicative);
                respuesta = 1;
            //AuditorSave(OperationsController.ApplicativeController_DeleteApplicative, "http//publiclogin", OperationsDescription.ApplicativeController_DeleteApplicative);

            }
            catch (Exception)
            {

                respuesta = 0;
            }

            return respuesta;
        }

        public void AuditorSave(string Consult, string URL, string Description)
        {
            try
            {
                int tipoUsuario, idusuario = 0;

                if (HttpContext == null)
                {
                    //    if (string.IsNullOrEmpty(HttpContext.Session.GetString("usuario")))
                    //{
                    idusuario = 11;
                    //tipoUsuario = 5;
                }
                else
                {
                    idusuario = int.Parse(HttpContext.Session.GetString("usuario"));
                   // tipoUsuario = int.Parse(HttpContext.Session.GetString("tipoUsuario"));

                }
                //if (string.IsNullOrEmpty(HttpContext.Session.GetString("tipoUsuario")))
                //{
                //    tipoUsuario = 99999;
                //}
                //else {

                //     tipoUsuario = int.Parse(HttpContext.Session.GetString("tipoUsuario"));

                //}


                DateTime CreationDate = DateTime.Now;

                Auditor auditor = new Auditor();
                auditor.Consult = Consult;
                auditor.CreationDate = CreationDate;
                auditor.Description = Description;
                //auditor.RoleId = tipoUsuario;
                auditor.UserId = idusuario;
                auditor.Url = URL;
                todoItemsAuditor.AddAuditor(auditor);

            }
            catch (Exception)
            {

                throw;
            }
            //,[Consult]
            //,[Url]
            //,[Description]

        }



        [HttpPost]
        [Route("api/Applicative/guardarDatosSeguridad")]
        public int guardarDatosSeguridad([FromBody]SecuritySaveViewModel securitySaveViewModel)
        {

            int rpta = 0;
            try
            {

               
                    if (securitySaveViewModel.id == "0")
                    {
                        ApplicativeRoleUserPage applicativeRoleUserPage = new ApplicativeRoleUserPage();
                        applicativeRoleUserPage.RoleId = int.Parse(securitySaveViewModel.idRol);
                        applicativeRoleUserPage.UserId = int.Parse(securitySaveViewModel.idUsuario);
                        applicativeRoleUserPage.AplicativeId =todoItemsApplicative.Applicatives.Where(t=>t.Name.ToUpper().Equals(securitySaveViewModel.idApplicative.ToUpper())).FirstOrDefault().Id;
                        applicativeRoleUserPage.PermissionId = todoItemPermiso.Permission.Where(t=>t.Message.ToUpper().Equals("Invitados".ToUpper())).FirstOrDefault().Id;

                        todoItemsApplicativeRolUserPage.AddApplicativeRoleUserPages(applicativeRoleUserPage);

                        string nombrerol = todoRol.Roles.Where(t => t.Id.ToString() == securitySaveViewModel.idRol).FirstOrDefault().Name;
                        string email= todoUsers.Usuarios.FirstOrDefault(t => t.id.ToString().Equals(securitySaveViewModel.idUsuario)).Email;
                        AuditorSave(OperationsController.ApplicativeController_SeguridadAlta, "http//publiclogin", OperationsDescription.ApplicativeController_SeguridadA + "applicativo:" + securitySaveViewModel.idApplicative + "Rol:" + nombrerol + "Usuario:" + email);

                       
                       

                          rpta = 1;
                    }
                    else
                    {
                        Page ApplicativesViewModel = todoItemsApplicativeSon.GetPagesById(int.Parse(securitySaveViewModel.id));

                        //ApplicativesViewModel.Name = ApplicativesViewModel.Name;

                        //ApplicativesViewModel.Description = ApplicativesViewModel.Description;
                        //ApplicativesViewModel.CategoriesId = ApplicatiViewModel.CategoriesId;
                        //ApplicativesViewModel.PermisoId = ApplicatiViewModel.PermisoId;

                        //ApplicativesViewModel.IsActive = true;

                        //todoItemsApplicativeSon.EditPages(ApplicativesViewModel);
                        //transaccion.Complete();
                        rpta = 1;
                        //AuditorSave(OperationsController.ApplicativeController_GuardarDatosson, "http//publiclogin", "Editar la pagina perteneiente a los aplicativos");

                    }

                
            }
            catch (Exception)
            {

                rpta = 0
                ;
            }
            return rpta;
        }



        

    }
}