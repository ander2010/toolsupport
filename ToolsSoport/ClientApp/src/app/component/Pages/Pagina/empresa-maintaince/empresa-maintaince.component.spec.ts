import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmpresaMaintainceComponent } from './empresa-maintaince.component';

describe('EmpresaMaintainceComponent', () => {
  let component: EmpresaMaintainceComponent;
  let fixture: ComponentFixture<EmpresaMaintainceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmpresaMaintainceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmpresaMaintainceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
