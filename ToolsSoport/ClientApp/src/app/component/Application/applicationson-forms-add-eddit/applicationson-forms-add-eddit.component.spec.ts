import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApplicationsonFormsAddEdditComponent } from './applicationson-forms-add-eddit.component';

describe('ApplicationsonFormsAddEdditComponent', () => {
  let component: ApplicationsonFormsAddEdditComponent;
  let fixture: ComponentFixture<ApplicationsonFormsAddEdditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApplicationsonFormsAddEdditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApplicationsonFormsAddEdditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
