import { Component, OnInit, Input } from '@angular/core';
import { UsuarioService } from '../../../services/usuario.service';
@Component({
  selector: 'list-rols',
  templateUrl: './list-rols.component.html',
  styleUrls: ['./list-rols.component.css']
})
export class ListRolsComponent implements OnInit {
  @Input() tipoUsuario: any;
  @Input() isMantenimiento = false;
  cabeceras: string[] = [ "Nombre", "Descripcion"]
  constructor(private usuarioservice: UsuarioService) { }

  ngOnInit() {

    this.usuarioservice.ListarTipoUsuario().subscribe(t => this.tipoUsuario = t);
    console.log(this.tipoUsuario);

  }

  eliminartipoUsuario(id) {

    this.usuarioservice.eliminarTipoUsuario(id).subscribe(t => {
      this.usuarioservice.ListarTipoUsuario().subscribe(t => this.tipoUsuario = t);
    });

  }
}
