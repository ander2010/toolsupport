﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ToolsSoport.Models.Rols
{
  public  interface IApplicativeUserRolesRepository
    {


        IEnumerable<ApplicativeUserRoles> ApplicativeUserRoles { get; }

        void AddApplicativeUserRol(ApplicativeUserRoles e);

        //void EditRol(UserRol e);

        //void DeleteRol(int RolID);
        //UserRol GetRolById(int RolID);
    }
}
