import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MaintenancePermissionComponent } from './maintenance-permission.component';

describe('MaintenancePermissionComponent', () => {
  let component: MaintenancePermissionComponent;
  let fixture: ComponentFixture<MaintenancePermissionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MaintenancePermissionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MaintenancePermissionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
