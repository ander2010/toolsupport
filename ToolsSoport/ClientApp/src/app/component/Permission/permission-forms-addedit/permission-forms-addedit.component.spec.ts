import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PermissionFormsAddeditComponent } from './permission-forms-addedit.component';

describe('PermissionFormsAddeditComponent', () => {
  let component: PermissionFormsAddeditComponent;
  let fixture: ComponentFixture<PermissionFormsAddeditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PermissionFormsAddeditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PermissionFormsAddeditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
