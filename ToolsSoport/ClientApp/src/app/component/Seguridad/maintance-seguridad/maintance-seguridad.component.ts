import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-maintance-seguridad',
  templateUrl: './maintance-seguridad.component.html',
  styleUrls: ['./maintance-seguridad.component.css']
})
export class MaintanceSeguridadComponent implements OnInit {
  nombrePagina: string = "Listado de Seguridad";
  tituloSuperior: string = "Adicione Seguridad a un Usuario";

  constructor() { }

  ngOnInit() {
  }

}
