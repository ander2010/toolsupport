﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ToolsSoport.Models.ApplicativeRoleUserPages
{
    public class ApplicativeRoleUserPageRepository : IApplicativeRoleUserPageRepository
    {
        ToolSoportContext context;
        public ApplicativeRoleUserPageRepository(ToolSoportContext ctx)
        {
            context = ctx;
        }


        public IEnumerable<ApplicativeRoleUserPage> ApplicativeRoleUserPages =>context.ApplicativeRoleUserPages;

        public void AddApplicativeRoleUserPages(ApplicativeRoleUserPage e)
        {
            context.ApplicativeRoleUserPages.Add(e);
            context.SaveChanges();
        }

        public void DeleteApplicativeRoleUserPages(int ApplicativeRoleUserPageID)
        {
            ApplicativeRoleUserPage e = new ApplicativeRoleUserPage() { Id = ApplicativeRoleUserPageID };
            context.ApplicativeRoleUserPages.Remove(e);

            context.SaveChanges();
        }

        public void EditApplicativeRoleUserPages(ApplicativeRoleUserPage e)
        {
            ApplicativeRoleUserPage dbEntry = context.ApplicativeRoleUserPages.FirstOrDefault(o => o.Id == e.Id);
            if (dbEntry != null)
            {
                // dbEntry.CreationDate = e.CreationDate;
                dbEntry.AplicativeId = e.AplicativeId;

                dbEntry.PermissionId = e.PermissionId;
                dbEntry.RoleId = e.RoleId;

                dbEntry.UserId = e.UserId;

                //dbEntry. = e.Description;
            }
        }

        public ApplicativeRoleUserPage GetApplicativeRoleUserPagesById(int ApplicativeRoleUserPageID)
        {
            ApplicativeRoleUserPage e = context.ApplicativeRoleUserPages.FirstOrDefault(t => t.Id == ApplicativeRoleUserPageID);
            return e;
        }


      
    }
}
