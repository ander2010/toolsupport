import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-maintance-user',
  templateUrl: './maintance-user.component.html',
  styleUrls: ['./maintance-user.component.css']
})
export class MaintanceUserComponent implements OnInit {
  nombrePagina: string = "Listado de Usuarios";
  tituloSuperior: string = "Adicione un nuevo Usuario";

  constructor() { }

  ngOnInit() {
  }

}
