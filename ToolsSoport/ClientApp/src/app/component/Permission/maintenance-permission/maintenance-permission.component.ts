import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-maintenance-permission',
  templateUrl: './maintenance-permission.component.html',
  styleUrls: ['./maintenance-permission.component.css']
})
export class MaintenancePermissionComponent implements OnInit {
  nombrePagina: string = "Listado de Permisos";
  tituloSuperior: string = "Adicione un nuevo permiso";
  constructor() { }

  ngOnInit() {
  }

}
