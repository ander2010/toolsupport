import { Component, OnInit, Input } from '@angular/core';
import { UsuarioService } from '../../../services/usuario.service';

@Component({
  selector: 'list-user',
  templateUrl: './list-user.component.html',
  styleUrls: ['./list-user.component.css']
})
export class ListUserComponent implements OnInit {
  @Input() Usuarios: any;
  @Input() isMantenimiento = false;
  constructor(private usuarioService: UsuarioService) { }

  ngOnInit() {
    this.usuarioService.getUsuario().subscribe(t => this.Usuarios = t);
  }

  eliminarUsuario(id) {
    this.usuarioService.eliminarUsuario(id).subscribe(t => {

      this.usuarioService.getUsuario().subscribe(t => this.Usuarios = t);
    });

  }
}
