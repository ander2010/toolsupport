﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ToolsSoport.Models.ApiRestViewModel
{
    public class AuditoryTypeViewModel
    {
        public int id { get; set; }
        public string tipo { get; set; }
    }
}
