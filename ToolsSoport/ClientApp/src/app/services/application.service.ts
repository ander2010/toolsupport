import { Injectable, Inject } from '@angular/core';
import { Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { Http } from '@angular/http';
import { Router } from '@angular/router';
import { securitySave } from '../component/Models/securitySave.model';

@Injectable()
export class ApplicationService {
   
  GetApplicativePage(parametro: string) {
    return this.http.get(this.urlBase + "api/Applicative/GetApplicativePage/" + parametro).map(t => t.json());
    }


  GetApplicative(parametro: string) {
    return this.http.get(this.urlBase + "api/Applicative/GetApplicative/" + parametro).map(t => t.json());
  }

  urlBase: string = "";
  constructor(private http: Http, @Inject("BASE_URL") urlbase: string, private route: Router) {
    this.urlBase = urlbase
  }

  saveUserRolApplicative(parametro,valor) {
    return this.http.get(this.urlBase + "api/Applicative/saveUserRolApplicative/" + parametro+"/"+valor).map(t => t.json());
  }
  





  public guardarDatosSeguridad(applicativoViewModel: securitySave) {

    const contentHeaders = new Headers();
    contentHeaders.append('Accept', 'application/json');
    contentHeaders.append('Content-Type', 'application/json');
    var UserFormulario = JSON.stringify(applicativoViewModel);

    return this.http.post(this.urlBase + "api/Applicative/guardarDatosSeguridad", UserFormulario, { headers: contentHeaders }).map(t => t.json());

  }

  public guardarDatos(applicativoViewModel) {

    return this.http.post(this.urlBase + "api/Applicative/guardarDatos", applicativoViewModel).map(t => t.json());

  }

  


  public ApplicativePageList() {
    return this.http.get(this.urlBase + "api/Applicative/GetPagesApplicative").map(t =>
      t.json()

    );
  }



  guardarDatosSon(applicativoViewModel) {
    return this.http.post(this.urlBase + "api/Applicative/GuardarDatosson", applicativoViewModel).map(t => t.json());
  }

  public EliminarApplicative(idApplicative) {

    return this.http.get(this.urlBase + "api/Applicative/DeleteApplicative/" + idApplicative).map(t => t.json());

  }

  public EliminarApplicativePage(idApplicative) {

    return this.http.get(this.urlBase + "api/Applicative/DeletePageApplicative/" + idApplicative).map(t => t.json());

  }

  public buscarEmpresaGrantor(grantorId: string) {

    return this.http.get(this.urlBase + "api/Applicative/DownloadString/" + grantorId).map(t => t.json());

  }


  

  public listarApplicative() {
    return this.http.get(this.urlBase + "api/Applicative/listarApplicative").map(t =>
      t.json()

    );
  }

  public DeleteCompany(idApplicative) {

    return this.http.get(this.urlBase + "api/Applicative/DeleteCompany/" + idApplicative).map(t => t.json());

  }



}
