import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { UsuarioService } from '../../../services/usuario.service';
import { ApplicationService } from '../../../services/application.service';
import { securitySave } from '../../Models/securitySave.model';

@Component({
  selector: 'app-seguridad-forms-addedit',
  templateUrl: './seguridad-forms-addedit.component.html',
  styleUrls: ['./seguridad-forms-addedit.component.css']
})
export class SeguridadFormsAddeditComponent implements OnInit {
  seguridad: FormGroup;
  roles: any;
  usuarios: any;
  Applicativos: any;
  versinUsuario: boolean = false; 
  CadenaRolValue: string = "";
  usuarioseleccionado: any;
  parametro: string = "";
  titulo: string = "Agregando seguridad";
  ver: boolean = true;
  constructor(private usuarioService: UsuarioService, private applicationservice: ApplicationService,
    private activateRoute: ActivatedRoute, private router: Router) {
    this.seguridad = new FormGroup({
      "id": new FormControl("0"),
     
      "idUsuario": new FormControl("0",[Validators.required]),
     
      "idApplicative": new FormControl("0", [Validators.required]),
     
      "idRol": new FormControl("0", [Validators.required]),

    });
  }
  ngOnInit() {

    this.usuarioService.gettipoUsuario().subscribe(t => this.roles = t);
    this.applicationservice.listarApplicative().subscribe(t => this.Applicativos = t);
    this.usuarioService.getUsuario().subscribe(t => this.usuarios = t);


    if (this.parametro == "id") {
      this.titulo = "Agregando  Seguridad"

    }
    else {
      this.titulo = "Agregando  Seguridad"
      //this.usuarioService.ListarpaginaRecuperadas(this.parametro).subscribe(t => {

      //  this.tipoUsuario.controls["description"].setValue(t.description);
      //  this.tipoUsuario.controls["name"].setValue(t.name);
      //  this.tipoUsuario.controls["id"].setValue(t.id);
      //  var listapaginas = t.listapagina.map(tt => tt.id);
      //  //pintar la información
      //  setTimeout(() => {
      //    var checks = document.getElementsByClassName("check");
      //    var ncheck = checks.length;
      //    var check;
      //    for (var i = 0; i < ncheck; i++) {
      //      check = checks[i];
      //      var indice = listapaginas.indexOf(check.name * 1);
      //      if (indice > -1) {
      //        check.checked = true;

      //      }
      //    }


        //}, 500);
        //    this.persona.controls["apMaterno"].setValue(t.apMaterno);
        //    this.persona.controls["telefono"].setValue(t.telefono);
        //    this.persona.controls["correo"].setValue(t.correo);
        //    this.persona.controls["fechanacimiento"].setValue(t.fechaCadena);

      }

  }

  guardarDatos() {

    if (this.seguridad.valid == true) {
      console.info(this.seguridad.value);
      
      this.applicationservice.guardarDatosSeguridad(this.seguridad.value).subscribe(t => {

        this.router.navigate(["/Seguridad"]);
      });
    }


  

  }

  UsuarioMostrar(tipofilter) {


    if (tipofilter == "") {
      console.info("Estavacio");

    }
    else {
      this.versinUsuario = true;
     // this.usuarioseleccionado = tipofilter.value;
      //this.seguridad.controls["idUsuario"].setValue(tipofilter.value)
      //console.info(tipofilter.value);
    }





  }

  AplicativosRoles(tipofilter, valor) {



    if (tipofilter == "") {
      //console.info("Estavacio");

    }
    else {
      this.CadenaRolValue = "aplicativo:" + tipofilter.value + "Rol:" + valor + "&&&";
      this.seguridad.controls["idApplicative"].setValue(this.CadenaRolValue)
      this.seguridad.controls["idRol"].setValue(this.CadenaRolValue)

      //if (this.seguridad.controls["idUsuario"].value!="0") {
        this.applicationservice.saveUserRolApplicative(1, this.seguridad.controls["idRol"].value).subscribe(t => {

          this.router.navigate(["/Auditor"]);
      });
      //}
     
      
    }
  }

}
