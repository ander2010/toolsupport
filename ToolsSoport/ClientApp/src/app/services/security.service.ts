import { Injectable, Inject } from '@angular/core';
import { Http } from '@angular/http';
import { Router } from '@angular/router';
import 'rxjs/add/operator/map';
@Injectable()
export class SecurityService {


  urlBase: string = "";
  constructor(private http: Http, @Inject("BASE_URL") urlbase: string, private route: Router) {
    this.urlBase = urlbase
  }

  public getUserRolAplicativePage() {
    return this.http.get(this.urlBase + "api/Users/getAllUserRolAplicativePage").map(t => t.json());
  }



  


  public eliminarUserRolAplicativePage(idusuario) {

    return this.http.get(this.urlBase + "api/Users/DeleteUser/" + idusuario).map(t => t.json());

  }

}
