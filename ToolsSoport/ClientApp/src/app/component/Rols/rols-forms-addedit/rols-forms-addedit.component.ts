import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UsuarioService } from '../../../services/usuario.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-rols-forms-addedit',
  templateUrl: './rols-forms-addedit.component.html',
  styleUrls: ['./rols-forms-addedit.component.css']
})
export class RolsFormsAddeditComponent implements OnInit {

  tipoUsuario: FormGroup;
  titulo: string;
  parametro: string;
  ListadoPaginas: any;
  constructor(private usuarioService: UsuarioService, private ruta: Router, private activateroute: ActivatedRoute) {
    this.tipoUsuario = new FormGroup({

      "id": new FormControl("0", Validators.required),
      "name": new FormControl("", Validators.required),
      "description": new FormControl("", Validators.required),
      "isActive": new FormControl("True", Validators.required),
      "valores": new FormControl(""),

    });
    this.activateroute.params.subscribe(parametro => {
      this.parametro = parametro["id"];
      if (this.parametro == "id") {
        this.titulo = "Agregando una nuevo rol"

      }
      else {

        this.titulo = "Editando un rol";


      }
    });
    this.usuarioService.listarPaginastipoUsuario().subscribe(t => this.ListadoPaginas = t);
  }

  ngOnInit() {
    if (this.parametro == "id") {
      this.titulo = "Agregando un rol"

    }
    else {

      this.usuarioService.ListarpaginaRecuperadas(this.parametro).subscribe(t => {

        this.tipoUsuario.controls["description"].setValue(t.description);
        this.tipoUsuario.controls["name"].setValue(t.name);
        this.tipoUsuario.controls["id"].setValue(t.id);
        var listapaginas = t.listapagina.map(tt => tt.id);
        //pintar la información
        setTimeout(() => {
          var checks = document.getElementsByClassName("check");
          var ncheck = checks.length;
          var check;
          for (var i = 0; i < ncheck; i++) {
            check = checks[i];
            var indice = listapaginas.indexOf(check.name * 1);
            if (indice > -1) {
              check.checked = true;

            }
          }


        }, 500);
        //    this.persona.controls["apMaterno"].setValue(t.apMaterno);
        //    this.persona.controls["telefono"].setValue(t.telefono);
        //    this.persona.controls["correo"].setValue(t.correo);
        //    this.persona.controls["fechanacimiento"].setValue(t.fechaCadena);

      });


    }
  }

  guardarDatos() {

    //if (this.tipoUsuario.controls["isActive"].value == "1") {
    //  this.tipoUsuario.controls["isActive"].setValue("True");
    //} else {

    //  this.tipoUsuario.controls["isActive"].setValue("False");
    //}
    this.usuarioService.guardarDatosTipoUsuario(this.tipoUsuario.value).subscribe(t => {

      this.ruta.navigate(['/Rols']);
    });



  }
  verCheck() {
    var seleccionados = "";
    var checks = document.getElementsByClassName("check");
    var check;
    for (var i = 0; i < checks.length; i++) {
      check = checks[i];
      if (check.checked == true) {
        seleccionados += check.name;
        seleccionados += "$";
      }
    }
    if (seleccionados != "") {
      seleccionados = seleccionados.substring(0, seleccionados.length - 1);

    }
    this.tipoUsuario.controls["valores"].setValue(seleccionados);
  }

}
