﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ToolsSoport.Models.Auditors
{

    [Table("AuditorLogs")]
    public class Auditor
    {

        [Key]
        public int Id { get; set; }
        public int RoleId { get; set; }
        public int UserId { get; set; }

        public DateTime CreationDate { get; set; }
        public string Consult { get; set; }


        public string Url { get; set; }

        public string Description { get; set; }
     
     
     
        



    }
}
