﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ToolsSoport.Models.ApiRestViewModel
{
    public class ServiceViewModel
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public string CrmId { get; set; }
            public int? Limit { get; set; }
    }
}