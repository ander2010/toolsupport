﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ToolsSoport.Models.Auditors
{
    public class AuditorRepository : IAuditorRepository
    {

        ToolSoportContext context;
        public AuditorRepository(ToolSoportContext ctx)
        {
            context = ctx;
        }
        public IEnumerable<Auditor> AuditorLog => context.Auditor;

        public void AddAuditor(Auditor e)
        {
            context.Auditor.Add(e);
            context.SaveChanges();
        }

        public void DeleteAuditor(int AuditorID)
        {
            Auditor e = new Auditor() { Id = AuditorID };
            context.Auditor.Remove(e);
            context.SaveChanges();
        }

        public void EditAuditor(Auditor e)
        {
            Auditor dbEntry = context.Auditor.FirstOrDefault(o => o.Id == e.Id);
            if (dbEntry != null)
            {
                dbEntry.UserId = e.UserId;
                dbEntry.Url = e.Url;
                dbEntry.RoleId = e.RoleId;
                dbEntry.Consult = e.Consult;
                dbEntry.Description = e.Description;
                dbEntry.CreationDate = e.CreationDate;
                //dbEntry. = e.Description;

            }
            context.SaveChanges();
        }

        public Auditor GetAuditorById(int AuditorID)
        {
            Auditor e = context.Auditor.FirstOrDefault(t => t.Id == AuditorID);
            return e;
        }
    }
}
