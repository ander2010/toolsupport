import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-maintenance-rols',
  templateUrl: './maintenance-rols.component.html',
  styleUrls: ['./maintenance-rols.component.css']
})
export class MaintenanceRolsComponent implements OnInit {

  nombrePagina: string = "Listado de Roles";
  tituloSuperior: string = "Adicione un rol";
  constructor() { }

  ngOnInit() {
  }

}
