﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ToolsSoport.Models.Permiso
{
    public class PermisoViewModel
    {
        public int Id { get; set; }
        public string Message { get; set; }
        public string Accion { get; set; }
        public bool IsActive { get; set; }
        public bool IsVisible { get; set; }

    }



}
