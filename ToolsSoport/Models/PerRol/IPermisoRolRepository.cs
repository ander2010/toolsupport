﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ToolsSoport.Models.PersonaRol
{
    public interface IPermisoRolRepository
    {

        IEnumerable<PermisosRol> PermisoRol { get; }

        void AddPermisoRol(PermisosRol e);

        void EditPermisoRol(PermisosRol e);

        void DeletePermisoRol(int permisoID);
        PermisosRol GetPermisoRolById(int permisoID);


    }
}
