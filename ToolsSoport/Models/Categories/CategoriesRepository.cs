﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ToolsSoport.Models.Categories
{
    public class CategoriesRepository : ICategoriesRespository
    {

        ToolSoportContext context;
        public CategoriesRepository(ToolSoportContext ctx)
        {
            context = ctx;
        }


        public IEnumerable<Categorie> Categories => context.Categories;

        public void AddCategories(Categorie e)
        {
            context.Categories.Add(e);
            context.SaveChanges();
        }

        public void DeleteCategories(int CateogoriesID)
        {
            Categorie e = new Categorie() { Id = CateogoriesID };
            context.Categories.Remove(e);
            context.SaveChanges();
        }

        public void EditCategories(Categorie e)
        {
            Categorie dbEntry = context.Categories.FirstOrDefault(o => o.Id == e.Id);
            if (dbEntry != null)
            {
                dbEntry.Name = e.Name;
                dbEntry.ApplicationId = e.ApplicationId;
               
                //dbEntry. = e.Description;

            }
            context.SaveChanges();
        }

        public Categorie GetCategoriesById(int CategoriesID)
        {
            Categorie e = context.Categories.FirstOrDefault(t => t.Id == CategoriesID);
            return e;
        }
    }
}
