﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ToolsSoport.Models.Usuarios
{
    public interface IUsuarioRepository
    {


        IEnumerable<Users> Usuarios { get; }

        void AddUser(Users e);

        void EditUser(Users e);

        void DeleteUser(int UserID);
        Users GetUserById(int UserID);
        bool login(string user, string pass);
    }
}
