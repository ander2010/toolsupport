import { Component, OnInit, Input } from '@angular/core';
import { SecurityService } from '../../../services/security.service';

@Component({
  selector: 'seguridad-list',
  templateUrl: './seguridad-list.component.html',
  styleUrls: ['./seguridad-list.component.css']
})
export class SeguridadListComponent implements OnInit {

 
  @Input() seguridad: any;
  @Input() isMantenimiento = false;
  constructor(private securityservice: SecurityService) { }

  ngOnInit() {
    this.securityservice.getUserRolAplicativePage().subscribe(t => this.seguridad = t);
  }

  eliminarUserRolAplicativePage(id) {
    this.securityservice.eliminarUserRolAplicativePage(id).subscribe(t => {

      this.securityservice.getUserRolAplicativePage().subscribe(t => this.seguridad = t);
    });

  }

}
