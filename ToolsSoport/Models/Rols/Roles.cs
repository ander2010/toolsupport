﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using ToolsSoport.Models.Usuarios;

namespace ToolsSoport.Models.Rols
{
    [Table(name:"Roles")]
    public class UserRol
    {
        [Key]
        public int Id { get; set; }

        [Required(ErrorMessage ="Esto  no puede ser Vacio")]
        public string Name { get; set; }

        public string Description { get; set; }

        public Boolean IsActive { get; set; }
        public DateTime CreationDate { get; set; }

       



    }
}
