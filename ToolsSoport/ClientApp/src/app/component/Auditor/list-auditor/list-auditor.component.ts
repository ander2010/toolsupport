import { Component, OnInit, Input } from '@angular/core';
import { AuditorService } from '../../../services/auditor.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'list-auditor',
  templateUrl: './list-auditor.component.html',
  styleUrls: ['./list-auditor.component.css']
})
export class ListAuditorComponent implements OnInit {

 
  @Input() auditoria: any;
  @Input() isMantenimiento = false;
  cabeceras: string[] = ["Id pagina", "Mensaje", "Accion"]
  constructor(private auditorService: AuditorService, private ruta: Router, private activateroute: ActivatedRoute) { }

  ngOnInit() {
    this.auditorService.listarAuditoria().subscribe(t => this.auditoria = t);
    console.info(this.auditoria)
  }




  eliminarAuditoria(id) {

    this.auditorService.EliminarAuditoria(id).subscribe(t => {

      this.auditorService.listarAuditoria().subscribe(t => this.auditoria = t);
    });

  }
}
