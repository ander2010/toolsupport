import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LogscdpListComponent } from './logscdp-list.component';

describe('LogscdpListComponent', () => {
  let component: LogscdpListComponent;
  let fixture: ComponentFixture<LogscdpListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LogscdpListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LogscdpListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
