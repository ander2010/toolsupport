import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { ApplicationService } from '../../../services/application.service';
import { Router, ActivatedRoute } from '@angular/router';
import { promise } from 'protractor';
import { resolve, reject } from 'q';




@Component({
  selector: 'application-forms-addedit',
  templateUrl: './application-forms-addedit.component.html',
  styleUrls: ['./application-forms-addedit.component.css']
})
export class ApplicationFormsAddeditComponent implements OnInit {

  applications: FormGroup;
  titulo: string;
  parametro: string;
  paremotridentificador: string;

  constructor(private applicationservice: ApplicationService, private ruta: Router, private activateroute: ActivatedRoute) {
    this.applications = new FormGroup({

      "id": new FormControl("0", Validators.required),
      "name": new FormControl("", Validators.required),
      "description": new FormControl("", Validators.required),
     

    });
    this.activateroute.params.subscribe(parametro => {
      this.parametro = parametro["id"];
      this.paremotridentificador = parametro["id"];
      if (this.parametro == "id") {
        this.titulo = "Agregando un  nuevo aplicativo"

      }
      else {

        this.titulo = "Editando un nuevo aplicativo";
        this.applicationservice.GetApplicative(this.parametro).subscribe(t => {

          this.applications.controls["description"].setValue(t.descripcion);
          this.applications.controls["name"].setValue(t.nombre);
          this.applications.controls["id"].setValue(t.id);

        });

      }
    });

  }

  ngOnInit() {
    if (this.parametro == "id") {
      this.titulo = "Agregando un rol"

    }
    else {
      this.titulo = "Editando Aplicativo"
      this.applicationservice.GetApplicative(this.parametro).subscribe(t => {

        this.applications.controls["description"].setValue(t.descripcion);
        this.applications.controls["name"].setValue(t.nombre);
        this.applications.controls["id"].setValue(t.id);

      });
      //this.applicationservice.EditadoDelApplicative(this.parametro).subscribe(t => {

      //  this.applications.controls["description"].setValue(t.descripcion);
      //  this.applications.controls["name"].setValue(t.nombre);
      //  this.applications.controls["id"].setValue(t.id);
      //  var Applications = t.listapagina.map(tt => tt.id);
        //pintar la información
        //setTimeout(() => {
        //  var checks = document.getElementsByClassName("check");
        //  var ncheck = checks.length;
        //  var check;
        //  for (var i = 0; i < ncheck; i++) {
        //    check = checks[i];
        //    var indice = 0
        //    if (indice > -1) {
        //      check.checked = true;

        //    }
        //  }


        //}, 500);
        //    this.persona.controls["apMaterno"].setValue(t.apMaterno);
        //    this.persona.controls["telefono"].setValue(t.telefono);
        //    this.persona.controls["correo"].setValue(t.correo);
        //    this.persona.controls["fechanacimiento"].setValue(t.fechaCadena);

      //});


    }
  }

  guardarDatos() {


    this.applicationservice.guardarDatos(this.applications.value).subscribe(t => {

      this.ruta.navigate(['/Application']);
    });



  }


}
