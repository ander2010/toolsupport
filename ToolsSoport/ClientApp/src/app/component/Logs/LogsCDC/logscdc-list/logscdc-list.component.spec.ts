import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LogscdcListComponent } from './logscdc-list.component';

describe('LogscdcListComponent', () => {
  let component: LogscdcListComponent;
  let fixture: ComponentFixture<LogscdcListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LogscdcListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LogscdcListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
