﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ToolsSoport.Models.ApiRestViewModel

{
    public class DocumentViewModel
    {

        public Guid uuid { get; set; }
        public string serieFolio { get; set; }
        public string Status { get; set; }

        public string StatusName { get; set; }
        public Guid CustomerId { get; set; }
        public string Customer { get; set; }
        public decimal TotalAmount { get; set; }
        public decimal Balance { get; set; }
        public string Currency { get; set; }
        public string LoadedDate { get; set; }
        public string DueDate { get; set; }
        public long? PaymentReference{get;set;} 
    }
}