import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageFormsAddeditComponent } from './page-forms-addedit.component';

describe('PageFormsAddeditComponent', () => {
  let component: PageFormsAddeditComponent;
  let fixture: ComponentFixture<PageFormsAddeditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageFormsAddeditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageFormsAddeditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
