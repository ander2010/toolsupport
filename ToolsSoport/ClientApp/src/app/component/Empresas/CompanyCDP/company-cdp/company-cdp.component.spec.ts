import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompanyCdpComponent } from './company-cdp.component';

describe('CompanyCdpComponent', () => {
  let component: CompanyCdpComponent;
  let fixture: ComponentFixture<CompanyCdpComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompanyCdpComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompanyCdpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
