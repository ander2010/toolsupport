﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using ToolsSoport.Models.Permiso;
using ToolsSoport.Models.Permiso;
using ToolsSoport.Models.Rols;

namespace ToolsSoport.Models.PersonaRol
{

    [Table("PermisoRol")]
    public class PermisosRol
    {
        public int id { get; set; }
        public int IdPage { get; set; }
        public int IdRol { get; set; }
        public bool IsActive { get; set; }

        //public Permisos IidpaginaNavigation { get; set; }
        //public UserRol IidRolNavigation { get; set; }

    }
}


//,[IdPage]
//      ,[IdRol]
//      ,[IsActive] 