import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MaintanceUserComponent } from './maintance-user.component';

describe('MaintanceUserComponent', () => {
  let component: MaintanceUserComponent;
  let fixture: ComponentFixture<MaintanceUserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MaintanceUserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MaintanceUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
