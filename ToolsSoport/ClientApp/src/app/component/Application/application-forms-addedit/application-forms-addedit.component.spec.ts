import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApplicationFormsAddeditComponent } from './application-forms-addedit.component';

describe('ApplicationFormsAddeditComponent', () => {
  let component: ApplicationFormsAddeditComponent;
  let fixture: ComponentFixture<ApplicationFormsAddeditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApplicationFormsAddeditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApplicationFormsAddeditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
