﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ToolsSoport.Models.Auditors
{
    public interface IAuditorRepository
    {

        IEnumerable<Auditor> AuditorLog { get; }

        void AddAuditor(Auditor e);

        void EditAuditor(Auditor e);

        void DeleteAuditor(int AuditorID);
        Auditor GetAuditorById(int AuditorID);
    }
}
