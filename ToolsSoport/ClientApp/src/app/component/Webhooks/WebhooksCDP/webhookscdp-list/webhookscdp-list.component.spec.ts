import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WebhookscdpListComponent } from './webhookscdp-list.component';

describe('WebhookscdpListComponent', () => {
  let component: WebhookscdpListComponent;
  let fixture: ComponentFixture<WebhookscdpListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WebhookscdpListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WebhookscdpListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
