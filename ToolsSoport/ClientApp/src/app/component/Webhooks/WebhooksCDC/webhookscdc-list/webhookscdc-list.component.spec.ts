import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WebhookscdcListComponent } from './webhookscdc-list.component';

describe('WebhookscdcListComponent', () => {
  let component: WebhookscdcListComponent;
  let fixture: ComponentFixture<WebhookscdcListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WebhookscdcListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WebhookscdcListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
