﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ToolsSoport.Models.ApplicativeRoleUserPages
{
  public  interface IApplicativeRoleUserPageRepository
    {

        IEnumerable<ApplicativeRoleUserPage> ApplicativeRoleUserPages { get; }

        void AddApplicativeRoleUserPages(ApplicativeRoleUserPage e);

        void EditApplicativeRoleUserPages(ApplicativeRoleUserPage e);

        void DeleteApplicativeRoleUserPages(int ApplicativeRoleUserPageID);
        ApplicativeRoleUserPage GetApplicativeRoleUserPagesById(int ApplicativeRoleUserPageID);


    }
}
