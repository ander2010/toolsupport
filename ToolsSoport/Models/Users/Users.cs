﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

using ToolsSoport.Models.Rols;

namespace ToolsSoport.Models.Usuarios
{
    [Table(name: "Users")]
    public class Users
    {

        public int id { get; set; }
        public string Email { get; set; }
        public string UserName { get; set; }
        public string Surname { get; set; }
        
        public string Password { get; set; }
        public bool IsActive { get; set; }
       

        public DateTime CreationDate { get; set; }
        public DateTime ModificationDate { get; set; }
        public string Name { get; set; }

        
        //public Persona IidpersonaNavigation { get; set; }
        // public Roles idRolNavigation { get; set; }





    }
}



//USE[Soportetools]
//GO

///****** Object:  Table [dbo].[Users]    Script Date: 24/03/2020 23:25:38 ******/
//SET ANSI_NULLS ON
//GO

//SET QUOTED_IDENTIFIER ON
//GO

//CREATE TABLE[dbo].[Users]
//(

//   [Id][int] IDENTITY(1,1) NOT NULL,

//  [Email] [varchar] (50) NOT NULL,

//   [UserName] [varchar] (max) NOT NULL,
//	[Name]
//[varchar]
//(max) NULL,

//    [Surname] [varchar]
//(max) NULL,

//    [IsActive] [bit] NULL,
//	[CreationDate] [datetime] NULL,
//	[ModificationDate] [datetime] NULL,
// CONSTRAINT[PK_Users] PRIMARY KEY CLUSTERED
//(
//   [Id] ASC
//)WITH(PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON[PRIMARY]
//) ON[PRIMARY] TEXTIMAGE_ON[PRIMARY]
//GO

