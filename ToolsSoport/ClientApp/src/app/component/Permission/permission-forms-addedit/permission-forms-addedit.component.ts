import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UsuarioService } from '../../../services/usuario.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-permission-forms-addedit',
  templateUrl: './permission-forms-addedit.component.html',
  styleUrls: ['./permission-forms-addedit.component.css']
})
export class PermissionFormsAddeditComponent implements OnInit {

  Permission: FormGroup;
  titulo: string;
  parametro: string;
  constructor(private usuarioSErvice: UsuarioService, private ruta: Router, private activateroute: ActivatedRoute) {
    this.Permission = new FormGroup({
      "id": new FormControl("0"),
      "message": new FormControl("", [Validators.required, Validators.maxLength(100)]),
      "accion": new FormControl("", [Validators.required, Validators.maxLength(100)]),
      "isActive": new FormControl("1", Validators.required),
      "isVisible": new FormControl("1"),


    });

    this.activateroute.params.subscribe(parametro => {
      this.parametro = parametro["id"];
      if (this.parametro == "id") {
        this.titulo = "Agregando una nueva Pagina"

      }
      else {

        this.titulo = "Editando una Pagina";


      }
    });
  }

  ngOnInit() {


    if (this.parametro == "id") {
      this.titulo = "Agregando una nueva Pagina"

    }
    else {

      this.usuarioSErvice.RecuperarPagina(this.parametro).subscribe(t => {

        if (t.accion == null) {
          this.ruta.navigate(['/no-encontro-informacion']);
        } else {
          this.Permission.controls["id"].setValue(t.id);
          this.Permission.controls["message"].setValue(t.message);
          this.Permission.controls["accion"].setValue(t.accion);
          this.Permission.controls["isVisible"].setValue(t.isVisible.toString());
        }
        //this.persona.controls["apMaterno"].setValue(t.apMaterno);
        //this.persona.controls["telefono"].setValue(t.telefono);
        //this.persona.controls["correo"].setValue(t.correo);
        //this.persona.controls["fechanacimiento"].setValue(t.fechaCadena);

      });


    }


  }
  guardarDatos() {
    console.info(this.Permission.value);
    this.usuarioSErvice.GuardarPagina(this.Permission.value).subscribe(t => { this.ruta.navigate(['/Pages']) });
  }

}
