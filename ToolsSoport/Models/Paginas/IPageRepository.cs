﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace ToolsSoport.Models.Paginas
{
    public interface IPageRepository
    {

        IEnumerable<Page> Pages { get; }

        void AddPages(Page e);

        void EditPages(Page e);

        void DeletePages(int PageID);
        Page GetPagesById(int PageID);
    }
}
